<?php

return [

    'retrieved' => ':model recuperado correctamente.',
    'saved'     => ':model guardado correctamente.',
    'updated'   => ':model actualizado correctamente.',
    'deleted'   => ':model borrado correctamente.',
    'not_found' => ':model no encontrado',

];
