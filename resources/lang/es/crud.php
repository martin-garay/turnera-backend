<?php

return [

    'add_new'      => 'Nuevo',
    'cancel'       => 'Cancelar',
    'save'         => 'Guardar',
    'edit'         => 'Editar',
    'detail'       => 'Detalle',
    'back'         => 'Volver',
    'action'       => 'Actción',
    'id'           => 'Id',
    'created_at'   => 'Creado el',
    'updated_at'   => 'Actualizado el',
    'deleted_at'   => 'Borrado el',
    'are_you_sure' => 'Esta seguro?',
];
