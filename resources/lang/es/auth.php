<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor inténtelo de nuevo en :seconds segundos.',

    'full_name'        => 'Nombre Completo',
    'email'            => 'Email',
    'password'         => 'Contraseña',
    'confirm_password' => 'Confirmación de contraseña',
    'remember_me'      => 'Recuerdame',
    'sign_in'          => 'Loguearme',
    'sign_out'         => 'Cerrar sesión',
    'register'         => 'Registrarme',

    'login' => [
        'title'               => 'Logueate para empezar la sesión',
        'forgot_password'     => 'Olvide mi contraseña',
        'register_membership' => 'Registrar nuevo usuario',
    ],

    'registration' => [
        'title'           => 'Registrar nuevo usuario',
        'i_agree'         => 'Estoy de acuerdo',
        'terms'           => 'los terminos y condiciones',
        'have_membership' => 'Ya tengo un usuario',
    ],

    'forgot_password' => [
        'title'          => 'Ingrese su mail para resetear su contraseña',
        'send_pwd_reset' => 'Enviar link de reseteo',
    ],

    'reset_password' => [
        'title'         => 'Resetear tu contraseña',
        'reset_pwd_btn' => 'Resetear contraseña',
    ],

    'confirm_passwords' => [
        'title'                => 'Por favor confirme si contraseña antes de continuar.',
        'forgot_your_password' => 'Olvido su contraseña?',
    ],

    'verify_email' => [
        'title'       => 'Revise su casilla de correo electronico',
        'success'     => 'Un nuevo link de verificacion ah sido enviado a su dirección de correo electronico',
        'notice'      => 'Antes de continuar, por favor chequee el link de verificacion en su email. Si no recibiste el mail,',
        'another_req' => 'click aquí para solicitar otro',
    ],

    'emails' => [
        'password' => [
            'reset_link' => 'Click here to reset your password',
        ],
    ],

    'app' => [
        'member_since' => 'Member since',
        'messages'     => 'Messages',
        'settings'     => 'Settings',
        'lock_account' => 'Lock Account',
        'profile'      => 'Profile',
        'online'       => 'Online',
        'search'       => 'Search',
        'create'       => 'Create',
        'export'       => 'Export',
        'print'        => 'Print',
        'reset'        => 'Reset',
        'reload'       => 'Reload',
    ],
];
