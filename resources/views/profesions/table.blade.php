<div class="table-responsive">
    <table class="table" id="profesions-table">
        <thead>
        <tr>
            <th>Descripción</th>
            <th>Activo</th>        
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($profesions as $profesion)
            <tr>
                <td>{{ $profesion->descripcion}}</td>
                <td>@if($profesion->activo) SI @else NO @endif </td>            
                <td width="120">
                    {!! Form::open(['route' => ['profesions.destroy', $profesion->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('profesions.show', [$profesion->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('profesions.edit', [$profesion->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
