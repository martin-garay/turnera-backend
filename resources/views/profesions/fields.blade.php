<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','maxlength' => 200,'required' => '']) !!}
</div>


<!-- Activo Field -->
<div class="form-group col-sm-12">
    
        @isset($activo)
            <input class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Activo" data-off="Deshabilitado" {{ $activo ? 'checked' : '' }}>
        @else
            <input class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Activo" data-off="Deshabilitado" checked>
        @endisset
    
</div>
