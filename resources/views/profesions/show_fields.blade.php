<!-- Descripcion	String,200 Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion	string,200', 'Descripción	String,200:') !!}
    <p>{{ $profesion->descripcion	string,200 }}</p>
</div>

<!-- Activo Field -->
<div class="col-sm-12">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{{ $profesion->activo }}</p>
</div>

<!-- Id Empresa Field -->
<div class="col-sm-12">
    {!! Form::label('id_empresa', 'Empresa:') !!}
    <p>{{ $profesion->id_empresa }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $profesion->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $profesion->updated_at->format('d/m/Y h:i')}}</p>
</div>

