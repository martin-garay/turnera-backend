<div class="table-responsive">
    <table class="table" id="sucursals-table">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Calle</th>
            <th>Altura</th>       
            <th>Localidad</th>
            <th>Teléfono Fijo</th>
            <th>Teléfono Celular</th>
            <th>Mail</th>
            <th>Casa Central</th>
            <th>Activo</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sucursals as $sucursal)
            <tr>
                <td>{{ $sucursal->nombre }}</td>
                <td>{{ $sucursal->calle }}</td>
                <td>{{ $sucursal->altura }}</td>                
                <td>{{ $sucursal->id_localidad }}</td>
                <td>{{ $sucursal->telefono_fijo }}</td>
                <td>{{ $sucursal->telefono_celular }}</td>
                <td>{{ $sucursal->mail }}</td>                        
                <td>@if($sucursal->casa_central) SI @else NO @endif </td>
                <td>@if($sucursal->activo) SI @else NO @endif </td>
                <td width="120">
                    {!! Form::open(['route' => ['sucursals.destroy', $sucursal->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('sucursals.show', [$sucursal->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('sucursals.edit', [$sucursal->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
