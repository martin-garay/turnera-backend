<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $sucursal->nombre }}</p>
</div>

<!-- Calle Field -->
<div class="col-sm-12">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{{ $sucursal->calle }}</p>
</div>

<!-- Altura Field -->
<div class="col-sm-12">
    {!! Form::label('altura', 'Altura:') !!}
    <p>{{ $sucursal->altura }}</p>
</div>

<!-- Departamento Field -->
<div class="col-sm-12">
    {!! Form::label('departamento', 'Departamento:') !!}
    <p>{{ $sucursal->departamento }}</p>
</div>

<!-- Piso Field -->
<div class="col-sm-12">
    {!! Form::label('piso', 'Piso:') !!}
    <p>{{ $sucursal->piso }}</p>
</div>

<!-- Id Localidad Field -->
<div class="col-sm-12">
    {!! Form::label('id_localidad', 'Localidad:') !!}
    <p>{{ $sucursal->id_localidad }}</p>
</div>

<!-- Telefono Fijo Field -->
<div class="col-sm-12">
    {!! Form::label('telefono_fijo', 'Teléfono Fijo:') !!}
    <p>{{ $sucursal->telefono_fijo }}</p>
</div>

<!-- Telefono Celular Field -->
<div class="col-sm-12">
    {!! Form::label('telefono_celular', 'Teléfono Celular:') !!}
    <p>{{ $sucursal->telefono_celular }}</p>
</div>

<!-- Mail Field -->
<div class="col-sm-12">
    {!! Form::label('mail', 'Mail:') !!}
    <p>{{ $sucursal->mail }}</p>
</div>

<!-- Id Empresa Field -->
<div class="col-sm-12">
    {!! Form::label('id_empresa', 'Empresa:') !!}
    <p>{{ $sucursal->id_empresa }}</p>
</div>

<!-- Orden Field -->
<div class="col-sm-12">
    {!! Form::label('orden', 'Orden:') !!}
    <p>{{ $sucursal->orden }}</p>
</div>

<!-- Casa Central Field -->
<div class="col-sm-12">
    {!! Form::label('casa_central', 'Casa Central:') !!}
    <p>{{ $sucursal->casa_central }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $sucursal->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $sucursal->updated_at->format('d/m/Y h:i')}}</p>
</div>

