<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 255]) !!}
</div>

<!-- Calle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calle', 'Calle:') !!}
    {!! Form::text('calle', null, ['class' => 'form-control','maxlength' => 255]) !!}
</div>

<!-- Altura Field -->
<div class="form-group col-sm-6">
    {!! Form::label('altura', 'Altura:') !!}
    {!! Form::number('altura', null, ['class' => 'form-control']) !!}
</div>

<!-- Departamento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('departamento', 'Departamento:') !!}
    {!! Form::text('departamento', null, ['class' => 'form-control','maxlength' => 20]) !!}
</div>

<!-- Piso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('piso', 'Piso:') !!}
    {!! Form::number('piso', null, ['class' => 'form-control','max' => 100]) !!}
</div>

<!-- Id Localidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_localidad', 'Localidad:') !!}
    {!! Form::number('id_localidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Fijo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_fijo', 'Teléfono Fijo:') !!}
    {!! Form::text('telefono_fijo', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Celular Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_celular', 'Teléfono Celular:') !!}
    {!! Form::text('telefono_celular', null, ['class' => 'form-control']) !!}
</div>

<!-- Mail Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mail', 'Mail:') !!}
    {!! Form::text('mail', null, ['class' => 'form-control']) !!}
</div>


<!-- Orden Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden', 'Orden:') !!}
    {!! Form::number('orden', null, ['class' => 'form-control']) !!}
</div>

<!-- Casa Central Field -->
<div class="form-group col-sm-12">
        {!! Form::label('casa_central', 'Casa Central') !!}
    
        @isset($activo)
            <input class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="SI" data-off="NO" {{ $casa_central ? 'checked' : '' }}>
        @else
            <input class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="SI" data-off="NO" checked>
        @endisset
    
</div>

<!-- Activo Field -->
<div class="form-group col-sm-12">
    
        @isset($activo)
            <input class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Activo" data-off="Deshabilitado" {{ $activo ? 'checked' : '' }}>
        @else
            <input class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Activo" data-off="Deshabilitado" checked>
        @endisset
    
</div>
