<div class="table-responsive">
    <table class="table" id="especialidads-table">
        <thead>
        <tr>
            <th>Descripción</th>
            <th>Activo</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($especialidads as $especialidad)
            <tr>
                <td>{{ $especialidad->descripcion }}</td>
                <td>@if($especialidad->activo) SI @else NO @endif</td>
                <td width="120">
                    {!! Form::open(['route' => ['especialidads.destroy', $especialidad->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('especialidads.show', [$especialidad->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('especialidads.edit', [$especialidad->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
