<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    <p>{{ $especialidad->descripcion }}</p>
</div>

<!-- Activo Field -->
<div class="col-sm-12">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{{ $especialidad->activo }}</p>
</div>

<!-- Id Empresa Field -->
<div class="col-sm-12">
    {!! Form::label('id_empresa', 'Empresa:') !!}
    <p>{{ $especialidad->id_empresa }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $especialidad->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $especialidad->updated_at->format('d/m/Y h:i')}}</p>
</div>

