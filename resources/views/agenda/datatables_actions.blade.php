{!! Form::open(['route' => ['agenda.destroy', $id], 'method' => 'delete']) !!}
<style type="text/css">    
    a .btn{
        padding: 0 !important;
        font-size: .9rem;
        border: 1px solid indianred;
        background: indianred;        
    }

    .btn > span {
        display: inline-flex;
        color: white;
    }

    .btn > span > p{
        margin-left: 2px;
        margin: 0;
        line-height: 1.2;
    }


@media (max-width:480px)  { 
/* smartphones, Android phones, landscape iPhone */ 
    .btn > span > p{
        display: none;
    }

    .dt-buttons{
        display: none;
    }
}
    
</style>

<div class='btn-group'>    
    <a href="{{ route('agenda.presente', $id) }}" class='btn btn-default btn-xs botones-accion' style="background: forestgreen;">
        <!-- <i class="fa fa-edit"></i> -->
        <span>
            <i class="fas fa-user"></i>
            <p>PRESENTE</p>
        </span>
    </a>
    <a href="{{ route('agenda.ausente', $id) }}" class='btn btn-default btn-xs botones-accion' style="background: indianred;">
        <!-- <i class="fa fa-eye"></i> -->
        <span>
            <i class="fas fa-user-slash"></i>
            <p>AUSENTE</p>
        </span>
    </a>
   
</div>
{!! Form::close() !!}
