<!-- Usuario Field -->
<div class="col-sm-12">
    {!! Form::label('usuario', 'Usuario:') !!}
    <p>{{ $listadoReservas->usuario }}</p>
</div>

<!-- Fecha Field -->
<div class="col-sm-12">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $listadoReservas->fecha }}</p>
</div>

<!-- Hora Field -->
<div class="col-sm-12">
    {!! Form::label('hora', 'Hora:') !!}
    <p>{{ $listadoReservas->hora }}</p>
</div>

<!-- Profesional Field -->
<div class="col-sm-12">
    {!! Form::label('profesional', 'Profesional:') !!}
    <p>{{ $listadoReservas->profesional }}</p>
</div>

<!-- Especialidad Field -->
<div class="col-sm-12">
    {!! Form::label('especialidad', 'Especialidad:') !!}
    <p>{{ $listadoReservas->especialidad }}</p>
</div>

<!-- Estado Field -->
<div class="col-sm-12">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{{ $listadoReservas->estado }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $listadoReservas->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $listadoReservas->updated_at }}</p>
</div>

