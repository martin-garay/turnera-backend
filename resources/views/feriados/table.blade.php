<div class="table-responsive">
    <table class="table" id="feriados-table">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Descripción</th>
            <th>Hora Desde</th>
            <th>Hora Hasta</th>
            <th>Sucursal</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($feriados as $feriado)
            <tr>
                <td>{{ $feriado->fecha->format('d/m/Y') }}</td>
                <td>{{ $feriado->descripcion }}</td>
                <td>{{ $feriado->hora_desde }}</td>
                <td>{{ $feriado->hora_hasta }}</td>
                <td>{{ $feriado->sucursal->nombre }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['feriados.destroy', $feriado->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('feriados.show', [$feriado->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('feriados.edit', [$feriado->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
