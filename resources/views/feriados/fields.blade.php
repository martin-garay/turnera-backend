<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','maxlength' => 200,'required' => '']) !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', isset($feriado) ? $feriado->fecha : null , ['class' => 'form-control','id'=>'fecha']) !!}
</div>

<!-- push('page_scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: true,
            sideBySide: true
        })
    </script>
endpush -->

<!-- Hora Desde Field -->
<div class="form-group col-sm-12">
    {!! Form::label('hora_desde', 'Hora Desde:') !!}
    {!! Form::time('hora_desde', null, ['class' => 'form-control']) !!}
</div>

<!-- Hora Hasta Field -->
<div class="form-group col-sm-12">
    {!! Form::label('hora_hasta', 'Hora Hasta:') !!}
    {!! Form::time('hora_hasta', null, ['class' => 'form-control']) !!}
</div>

<!-- Sucursal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_sucursal', 'Sucursal:') !!}
    {!! Form::select('id_sucursal', $sucursales, null, ['class' => 'form-control custom-select']) !!}
</div>