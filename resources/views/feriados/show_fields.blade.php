<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    <p>{{ $feriado->descripcion }}</p>
</div>


<!-- Fecha Field -->
<div class="col-sm-12">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $feriado->fecha }}</p>
</div>

<!-- Hora Desde Field -->
<div class="col-sm-12">
    {!! Form::label('hora_desde', 'Hora Desde:') !!}
    <p>{{ $feriado->hora_desde }}</p>
</div>

<!-- Hora Hasta Field -->
<div class="col-sm-12">
    {!! Form::label('hora_hasta', 'Hora Hasta:') !!}
    <p>{{ $feriado->hora_hasta }}</p>
</div>

<!-- Id Sucursal Field -->
<div class="col-sm-12">
    {!! Form::label('id_sucursal', 'Sucursal:') !!}
    <p>{{ $feriado->id_sucursal }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $feriado->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $feriado->updated_at->format('d/m/Y h:i')}}</p>
</div>

