<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    <p>{{ $rubro->descripcion }}</p>
</div>

<!-- Activo Field -->
<div class="col-sm-12">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{{ $rubro->activo }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $rubro->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $rubro->updated_at->format('d/m/Y h:i')}}</p>
</div>

