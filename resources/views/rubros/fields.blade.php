<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','maxlength' => 200,'required' => '']) !!}
</div>

<!-- Activo Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('activo', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('activo', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('activo', 'Activo', ['class' => 'form-check-label']) !!}
    </div>
</div>
