@isset($reserva)
    @livewire('especialidad-profesional-turno', [
        'selectedProfesional' => $reserva->turno->id_profesional,
        'selectedEspecialidad' => $reserva->turno->id_especialidad,
        'turno' => $reserva->turno,        
    ])
@else
    @livewire('especialidad-profesional-turno')
@endisset


<!-- Id Usuario Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_usuario', 'Usuario:') !!}
    {!! Form::select('id_usuario', $usuarios, null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>
