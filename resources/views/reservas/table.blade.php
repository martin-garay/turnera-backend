<div class="table-responsive">
    <table class="table" id="reservas-table">
        <thead>
        <tr>
            <th>Estado</th>
            <th>Usuario</th>
            <th>Especialidad</th>
            <th>Profesional</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reservas as $reserva)
            <tr>
                <td>{{ $reserva->estado }}</td>
                <td>{{ $reserva->usuario->name }}</td>
                <td>{{ $reserva->turno->especialidad->descripcion }}</td>
                <td>{{ $reserva->turno->profesional->apellido }} {{ $reserva->turno->profesional->nombre }}</td>                
                <td>{{ $reserva->turno->fecha->format('d/m/Y') }}</td>
                <td>{{ $reserva->turno->hora_desde }}</td>
                <!-- <td width="120">
                    {!! Form::open(['route' => ['reservas.destroy', $reserva->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reservas.show', [$reserva->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('reservas.edit', [$reserva->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td> -->
                <td width="120">
                    {!! Form::open(['route' => ['reservas.cancelar', $reserva->id], 'method' => 'post']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reservas.show', [$reserva->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        @if(!$reserva->estado->equals(App\Models\Estados\Reserva\Cancelada::class) )
                        <a href="{{ route('reservas.edit', [$reserva->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        
                        {!! Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro que quiere cancelar la reserva?')"]) !!}
                        @endif
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
