<!-- Id Usuario Field -->
<div class="col-sm-12">
    {!! Form::label('id_usuario', 'Usuario:') !!}
    <p>{{ $reserva->usuario->name }}</p>
</div>

<!-- Observaciones Field -->
<div class="col-sm-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{{ $reserva->observaciones }}</p>
</div>

<!-- Fecha Field -->
<div class="col-sm-12">
    {!! Form::label('id_turno', 'Fecha:') !!}
    <p>{{ $reserva->turno->fecha->format('d/m/Y') }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('hora', 'Hora:') !!}
    <p>{{ $reserva->turno->hora_desde }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Fecha de creación:') !!}
    <p>{{ $reserva->created_at->format('d/m/Y H:m') }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Ultima actualización:') !!}
    <p>{{ $reserva->updated_at->format('d/m/Y H:m') }}</p>
</div>

