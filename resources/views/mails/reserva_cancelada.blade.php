<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>{{ config('app.name') }} | Turno Cancelado</title>
</head>
<body>
    <h1>Cancelación exitosa</h1>
    <p>Datos de su turno:</p>
    <ul>
        <li>Fecha: {{ $reserva->turno->fecha_formateada }}</li>
        <li>Hora: {{ $reserva->turno->hora_desde_formateada }}</li>
        <li>Profesional: {{ $reserva->turno->profesional->apellido }} {{ $reserva->turno->profesional->nombre }}</li>
        <li>Especialidad: {{ $reserva->turno->especialidad->descripcion }}</li>
    </ul>    
</body>
</html>