<div class="table-responsive">
    <table class="table" id="parametrizacionTurnos-table">
        <thead>
        <tr class="table-primary">
        	
            <th>Hora Desde</th>
            <th>Hora Hasta</th>
            <th>Estado</th>
            <th>Sobreturnos</th>                        
            <th>Fecha Generación</th>
        </tr>
        </thead>
        <tbody>
        <!-- GroupBy Collection ref: https://stackoverflow.com/a/38632166 -->

        @foreach($turnos->groupBy('fechaText') as $fechas)
            <tr class="table-secondary">
            	
                <td colspan="6"><strong>#{{ $loop->index+1 }}</strong> Fecha: {{ $fechas[0]['fecha']->format('d/m/Y l') }}</td>                            	
            </tr>            	
            @foreach($fechas as $turno)
            <tr>
                <td>{{ $turno->hora_desde }}</td>
                <td>{{ $turno->hora_hasta }}</td>                
                <td>{{ $turno->id_estado }}</td>
                <td>{{ $turno->sobreturnos }}</td>
                <td>{{ $turno->fecha_generacion }}</td>                
            	
            </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
</div>
