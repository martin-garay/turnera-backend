@isset($parametrizacionTurno)
    @if($parametrizacionTurno->generado)
        <!-- <p class="text-primary">GENERADO</p> -->
        <div class="p-3 mb-2 bg-success text-white">TURNOS GENERADOS</div>
    @else        
        <div class="p-3 mb-2 bg-success text-white">TURNOS SIN GENERAR</div>
    @endif
@endisset
<div class="form-group col-sm-12">
    <label class="control-label" for="semana">Dias:</label>

    @isset($parametrizacionTurno)
        @livewire('selector-dias-semana', [ 'selectedDays' => $parametrizacionTurno->dias ])
    @else
        @livewire('selector-dias-semana')
    @endisset
</div>

<!-- Fecha Desde Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_desde', 'Fecha Desde:') !!}
    @isset($parametrizacionTurno)
        {!! Form::date('fecha_desde', $parametrizacionTurno->fecha_desde, ['class' => 'form-control','id'=>'fecha_desde', 'min'=>date('Y-m-d')]) !!}
    @else
        {!! Form::date('fecha_desde', null, ['class' => 'form-control','id'=>'fecha_desde', 'min'=>date('Y-m-d')]) !!}
    @endisset
</div>

<!-- Fecha Hasta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_hasta', 'Fecha Hasta:') !!}
    @isset($parametrizacionTurno)
        {!! Form::date('fecha_hasta', $parametrizacionTurno->fecha_hasta, ['class' => 'form-control','id'=>'fecha_hasta', 'min'=>date('Y-m-d')]) !!}
    @else
        {!! Form::date('fecha_hasta', null, ['class' => 'form-control','id'=>'fecha_hasta', 'min'=>date('Y-m-d')]) !!}
    @endisset
</div>

<!-- Hora Desde Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hora_desde', 'Hora Desde:') !!}
    {!! Form::time('hora_desde', null, ['class' => 'form-control']) !!}
</div>

<!-- Hora Hasta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hora_hasta', 'Hora Hasta:') !!}
    {!! Form::time('hora_hasta', null, ['class' => 'form-control']) !!}
</div>

<!-- Duracion Turno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duracion_turno', 'Duración del Turno:') !!}
    {!! Form::number('duracion_turno', null, ['class' => 'form-control']) !!}
</div>

<!-- Sobreturnos Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('sobreturnos', 'Sobreturnos:') !!}
    {!! Form::number('sobreturnos', null, ['class' => 'form-control']) !!}
</div>
 -->

<!-- Id Sucursal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_sucursal', 'Sucursal:') !!}    
    {!! Form::select('id_sucursal', $sucursales, null, ['class' => 'form-control custom-select']) !!}
</div>

@isset($parametrizacionTurno)
    @livewire('profesional-especialidad', [
        'selectedProfesional' => $parametrizacionTurno->id_profesional,
        'selectedEspecialidad' => $parametrizacionTurno->id_especialidad,        
    ])
@else
    @livewire('profesional-especialidad')
@endisset