@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Previsualización de Turnos a generar</h1>
                </div>
                <div class="col-sm-6">
                    @if(!$parametrizacionTurno->generado) 
                    <a class="btn btn-primary float-right"
                       href="{{ route('parametrizacionTurnos.generarTurnos', [$parametrizacionTurno->id]) }}">
                        Generar
                    </a>
                    @endif
                    <a class="btn btn-default float-right"
                       href="{{ route('parametrizacionTurnos.index') }}">
                        Volver
                    </a>                                        
                    
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">            
            
            <!-- Fecha Desde Field -->
            <div class="col-sm-12">
                {!! Form::label('estado', 'Estado Parametrización:') !!}
                @if($parametrizacionTurno->generado) 
                    <span class="text-success">GENERADO</span> 
                @else 
                    <span class="text-danger">SIN GENERAR</span>
                @endif                                
            </div>

            <!-- Fecha Desde Field -->
            <div class="col-sm-12">
                {!! Form::label('hora_desde', 'Fecha Desde:') !!}
                {{ $parametrizacionTurno->fecha_desde->format('d/m/Y') }}
            </div>

             <!-- Fecha Hasta Field -->
            <div class="col-sm-12">
                {!! Form::label('hora_desde', 'Fecha Hasta:') !!}
                {{ $parametrizacionTurno->fecha_hasta->format('d/m/Y') }}
            </div>


            <!-- Sucursal Field -->
            <div class="col-sm-12">
                {!! Form::label('fecha_desde', 'Sucursal:') !!}
                {{ $parametrizacionTurno->sucursal->nombre }}
            </div>

            <!-- Profesional Field -->
            <div class="col-sm-12">
                {!! Form::label('fecha_hasta', 'Profesional:') !!}
                {{ $parametrizacionTurno->profesional->full_name }}
            </div>

            <!-- Especialidad Field -->
            <div class="col-sm-12">
                {!! Form::label('hora_desde', 'Especialidad:') !!}
                {{ $parametrizacionTurno->especialidad->descripcion }}
            </div>

             <!-- Duracion Field -->
            <div class="col-sm-12">
                {!! Form::label('hora_desde', 'Duración:') !!}
                {{ $parametrizacionTurno->duracion_turno }}
            </div>           

            <div class="col-sm-12">
                @livewire('selector-dias-semana', [ 'selectedDays' => $parametrizacionTurno->dias, 'editable' => false ])
            </div>

            <!-- Cant Dias Field -->
            <div class="col-sm-12">
                {!! Form::label('cantidad_dias', 'Cantidad de días a generar:') !!}
                {{ $turnos->groupBy('fechaText')->count() }}
            </div>

            <div class="card-body p-0">
                @include('parametrizacion_turnos.table_turnos_agrupado')

                <div class="card-footer clearfix">
                    <div class="float-right">
                        
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
