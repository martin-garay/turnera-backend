@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Editar Parametrización de Turno</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($parametrizacionTurno, ['route' => ['parametrizacionTurnos.update', $parametrizacionTurno->id], 'method' => 'patch']) !!}

            <div class="card-body">
                <div class="row">
                    @include('parametrizacion_turnos.fields')
                </div>
            </div>

            <div class="card-footer">
            @if(!$parametrizacionTurno->generado)
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
            @endif
                <a href="{{ route('parametrizacionTurnos.index') }}" class="btn btn-default">Cancelar</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
