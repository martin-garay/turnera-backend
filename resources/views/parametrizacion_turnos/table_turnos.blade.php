<div class="table-responsive">
    <table class="table" id="parametrizacionTurnos-table">
        <thead>
        <tr>
        	<th>#</th>
            <th>Fecha</th>
            <th>Hora Desde</th>
            <th>Hora Hasta</th>
            <th>Estado</th>
            <th>Sobreturnos</th>                        
            <th>Fecha Generación</th>
        </tr>
        </thead>
        <tbody>
        @foreach($turnos as $turno)
            <tr>
            	<td>{{ $loop->index+1 }}</td>
                <td>{{ $turno->fecha->format('d/m/Y') }}</td>                
                <td>{{ $turno->hora_desde }}</td>
                <td>{{ $turno->hora_hasta }}</td>                
                <td>{{ $turno->estado }}</td>
                <td>{{ $turno->sobreturnos }}</td>
                <td>{{ $turno->fecha_generacion }}</td>                
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
