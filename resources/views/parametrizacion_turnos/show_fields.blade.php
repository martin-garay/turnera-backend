<!-- Fecha Desde Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_desde', 'Fecha Desde:') !!}
    <p>{{ $parametrizacionTurno->fecha_desde }}</p>
</div>

<!-- Fecha Hasta Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_hasta', 'Fecha Hasta:') !!}
    <p>{{ $parametrizacionTurno->fecha_hasta }}</p>
</div>

<!-- Hora Desde Field -->
<div class="col-sm-12">
    {!! Form::label('hora_desde', 'Hora Desde:') !!}
    <p>{{ $parametrizacionTurno->hora_desde }}</p>
</div>

<!-- Hora Hasta Field -->
<div class="col-sm-12">
    {!! Form::label('hora_hasta', 'Hora Hasta:') !!}
    <p>{{ $parametrizacionTurno->hora_hasta }}</p>
</div>

<!-- Duracion Turno Field -->
<div class="col-sm-12">
    {!! Form::label('duracion_turno', 'Duración del Turno:') !!}
    <p>{{ $parametrizacionTurno->duracion_turno }}</p>
</div>

<!-- Sobreturnos Field -->
<div class="col-sm-12">
    {!! Form::label('sobreturnos', 'Sobreturnos:') !!}
    <p>{{ $parametrizacionTurno->sobreturnos }}</p>
</div>

<!-- Id Sucursal Field -->
<div class="col-sm-12">
    {!! Form::label('id_sucursal', 'Sucursal:') !!}
    <p>{{ $parametrizacionTurno->id_sucursal }}</p>
</div>

<!-- Id Profesional Field -->
<div class="col-sm-12">
    {!! Form::label('id_profesional', 'Profesional:') !!}
    <p>{{ $parametrizacionTurno->id_profesional }}</p>
</div>

<!-- Id Especialidad Field -->
<div class="col-sm-12">
    {!! Form::label('id_especialidad', 'Especialidad:') !!}
    <p>{{ $parametrizacionTurno->id_especialidad }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $parametrizacionTurno->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $parametrizacionTurno->updated_at->format('d/m/Y h:i')}}</p>
</div>

