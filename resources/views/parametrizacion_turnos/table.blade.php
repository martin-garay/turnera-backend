<div class="table-responsive">
    <table class="table" id="parametrizacionTurnos-table">
        <thead>
        <tr>
            <th>Días</th>
            <th>Fecha Desde</th>
            <th>Fecha Hasta</th>
            <th>Hora Desde</th>
            <th>Hora Hasta</th>
            <th>Duración del Turno</th>            
            <th>Sucursal</th>
            <th>Profesional</th>
            <th>Especialidad</th>
            <th>Generado</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($parametrizacionTurnos as $parametrizacionTurno)
            <tr>
                <td>
                    <ul>
                    @foreach($parametrizacionTurno->dias() as $dia)
                        <li>
                            {{ $dia->abreviatura }}
                            @if(!$loop->last)
                            <!-- / -->
                            @endif
                        </li>
                    @endforeach
                    </ul>
                </td>
                <td>{{ $parametrizacionTurno->fecha_desde->format('d/m/Y') }}</td>
                <td>{{ $parametrizacionTurno->fecha_hasta->format('d/m/Y') }}</td>
                <td>{{ Str::limit($parametrizacionTurno->hora_desde, 5, '') }}</td>
                <td>{{ Str::limit($parametrizacionTurno->hora_hasta, 5, '') }}</td>
                <td>{{ $parametrizacionTurno->duracion_turno }}</td>                
                <td>{{ $parametrizacionTurno->sucursal->nombre }}</td>
                <td>{{ $parametrizacionTurno->profesional->full_name }}</td>
                <td>{{ $parametrizacionTurno->especialidad->descripcion }}</td>
                <td>@if($parametrizacionTurno->generado)
                        <p class="text-success">SI</p> 
                    @else 
                        <p class="text-danger">NO</p>
                    @endif
                </td>
                <td width="120">
                    {!! Form::open(['route' => ['parametrizacionTurnos.destroy', $parametrizacionTurno->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('parametrizacionTurnos.show', [$parametrizacionTurno->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('parametrizacionTurnos.edit', [$parametrizacionTurno->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        <a href="{{ route('parametrizacionTurnos.visualizarTurnos', [$parametrizacionTurno->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="fas fa-cog" title="Generar"></i>
                        </a>                        
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
