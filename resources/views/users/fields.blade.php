<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Nombre') !!}
    {!! Form::text('name', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-12">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password', ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Confirmation Password Field -->
<div class="form-group col-sm-12">
      {!! Form::label('password', 'Confirmación Password') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control col-sm-6']) !!}
</div>
