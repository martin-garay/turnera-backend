<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $empresa->nombre }}</p>
</div>

<!-- Id Rubro Field -->
<div class="col-sm-12">
    {!! Form::label('id_rubro', 'Rubro:') !!}
    <p>{{ $empresa->id_rubro }}</p>
</div>

<!-- Logo Field -->
<div class="col-sm-12">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ $empresa->logo }}</p>
</div>

<!-- Cuit Field -->
<div class="col-sm-12">
    {!! Form::label('cuit', 'Cuit:') !!}
    <p>{{ $empresa->cuit }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $empresa->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $empresa->updated_at->format('d/m/Y h:i')}}</p>
</div>

