<!-- Id Sucursal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_sucursal', 'Sucursal:') !!}    
    {!! Form::select('id_sucursal', $sucursales, null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Tipo de Instalación Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_tipo_instalacion', 'Tipo de Instalación:') !!}
    {!! Form::select('id_tipo_instalacion', $tipos_instalacion, null, ['class' => 'form-control custom-select']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Hora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor_hora', 'Valor por Hora:') !!}
    {!! Form::number('valor_hora', null, ['class' => 'form-control']) !!}
</div>

<!-- Capacidad Personas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('capacidad_personas', 'Capacidad Personas:') !!}
    {!! Form::number('capacidad_personas', null, ['class' => 'form-control','min' => 0]) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>
