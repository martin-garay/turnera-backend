<div class="table-responsive">
    <table class="table" id="instalacions-table">
        <thead>
        <tr>
            <th>Descripción</th>
            <th>Valor Hora</th>
            <th>Capacidad Personas</th>
            <th>Observaciones</th>
            <th>Tipo de Instalación</th>
            <th>Sucursal</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($instalacions as $instalacion)
            <tr>
            <td>{{ $instalacion->descripcion }}</td>
            <td>{{ $instalacion->valor_hora }}</td>
            <td>{{ $instalacion->capacidad_personas }}</td>
            <td>{{ $instalacion->observaciones }}</td>
            <td>{{ $instalacion->tipo_instalacion->descripcion }}</td>
            <td>{{ $instalacion->sucursal->nombre }}</td>            
                <td width="120">
                    {!! Form::open(['route' => ['instalacions.destroy', $instalacion->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('instalacions.show', [$instalacion->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('instalacions.edit', [$instalacion->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
