<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Sucursal:') !!}
    <p>{{ $instalacion->sucursal->nombre }}</p>
</div>

<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripción:') !!}
    <p>{{ $instalacion->descripcion }}</p>
</div>

<!-- Valor Hora Field -->
<div class="col-sm-12">
    {!! Form::label('valor_hora', 'Valor Hora:') !!}
    <p>{{ $instalacion->valor_hora }}</p>
</div>

<!-- Capacidad Personas Field -->
<div class="col-sm-12">
    {!! Form::label('capacidad_personas', 'Capacidad Personas:') !!}
    <p>{{ $instalacion->capacidad_personas }}</p>
</div>

<!-- Observaciones Field -->
<div class="col-sm-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{{ $instalacion->observaciones }}</p>
</div>

<!-- Id Tipo Instalacion Field -->
<div class="col-sm-12">
    {!! Form::label('id_tipo_instalacion', 'Tipo de Instalación:') !!}
    <p>{{ $instalacion->tipo_instalacion->descripcion}}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $instalacion->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $instalacion->updated_at }}</p>
</div>