@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7" style="margin-top: 2%">
                <div class="box">
                    <h3 class="box-title" style="padding: 2%">Verifica tu email</h3>

                    <div class="box-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                Se envio un nuevo link de verificación a tu email
                            </div>
                        @endif
                        <p>
                            Antes de continuar, busca en tu casilla de email el link de verificación.
                            Si no recibiste el mail,                            
                        </p>
                        <a href="{{ route('verification.resend') }}">click aquí para solicitar otro'</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection