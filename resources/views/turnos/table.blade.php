<div class="table-responsive">
    <table class="table" id="turnos-table">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Hora Desde</th>
            <th>Hora Hasta</th>
            <th>Estado</th>
            <th>Fecha Generación</th>
            <th>Profesional</th>
            <th>Especialidad</th>            
            <!-- <th>Sobreturnos</th> -->
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($turnos as $turno)
            <tr>
            <td>{{ $turno->fecha->format('d/m/Y') }}</td>
            <td>{{ $turno->hora_desde }}</td>
            <td>{{ $turno->hora_hasta }}</td>
            <td>{{ $turno->estado }}</td>
            <td>{{ $turno->fecha_generacion->format('d/m/Y') }}</td>
            <td>{{ $turno->profesional->full_name }}</td>
            <td>{{ $turno->especialidad->descripcion }}</td>            
            <!-- <td>{{ $turno->sobreturnos }}</td> -->
                <td width="120">
                    {!! Form::open(['route' => ['turnos.destroy', $turno->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('turnos.show', [$turno->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('turnos.edit', [$turno->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
