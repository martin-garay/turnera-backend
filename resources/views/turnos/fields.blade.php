<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::text('fecha', null, ['class' => 'form-control','id'=>'fecha']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Id Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_estado', 'Estado:') !!}
    {!! Form::number('id_estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Generacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_generacion', 'Fecha Generación:') !!}
    {!! Form::text('fecha_generacion', null, ['class' => 'form-control','id'=>'fecha_generacion']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha_generacion').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Id Profesional Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_profesional', 'Profesional:') !!}
    {!! Form::number('id_profesional', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Especialidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_especialidad', 'Especialidad:') !!}
    {!! Form::number('id_especialidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Parametrizacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_parametrizacion', 'Parametrización:') !!}
    {!! Form::number('id_parametrizacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Sobreturnos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sobreturnos', 'Sobreturnos:') !!}
    {!! Form::number('sobreturnos', null, ['class' => 'form-control']) !!}
</div>