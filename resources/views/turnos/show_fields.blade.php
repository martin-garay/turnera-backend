<!-- Fecha Field -->
<div class="col-sm-12">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{{ $turno->fecha }}</p>
</div>

<!-- Hora Desde Field -->
<div class="col-sm-12">
    {!! Form::label('hora_desde', 'Hora Desde:') !!}
    <p>{{ $turno->hora_desde }}</p>
</div>

<!-- Hora Hasta Field -->
<div class="col-sm-12">
    {!! Form::label('hora_hasta', 'Hora Hasta:') !!}
    <p>{{ $turno->hora_hasta }}</p>
</div>

<!-- Id Estado Field -->
<div class="col-sm-12">
    {!! Form::label('id_estado', 'Estado:') !!}
    <p>{{ $turno->id_estado }}</p>
</div>

<!-- Fecha Generacion Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_generacion', 'Fecha Generación:') !!}
    <p>{{ $turno->fecha_generacion }}</p>
</div>

<!-- Id Profesional Field -->
<div class="col-sm-12">
    {!! Form::label('id_profesional', 'Profesional:') !!}
    <p>{{ $turno->id_profesional }}</p>
</div>

<!-- Id Especialidad Field -->
<div class="col-sm-12">
    {!! Form::label('id_especialidad', 'Especialidad:') !!}
    <p>{{ $turno->id_especialidad }}</p>
</div>

<!-- Id Parametrizacion Field -->
<div class="col-sm-12">
    {!! Form::label('id_parametrizacion', 'Parametrización:') !!}
    <p>{{ $turno->id_parametrizacion }}</p>
</div>

<!-- Sobreturnos Field -->
<div class="col-sm-12">
    {!! Form::label('sobreturnos', 'Sobreturnos:') !!}
    <p>{{ $turno->sobreturnos }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $turno->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $turno->updated_at->format('d/m/Y h:i')}}</p>
</div>

