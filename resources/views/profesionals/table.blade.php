<div class="table-responsive">
    <table class="table" id="profesionals-table">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Matrícula</th>
            <th>Dni</th>
            <th>Calle</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($profesionals as $profesional)
            <tr>
                <td>{{ $profesional->nombre }}</td>
            <td>{{ $profesional->apellido }}</td>
            <td>{{ $profesional->matricula }}</td>
            <td>{{ $profesional->dni }}</td>
            <td>{{ $profesional->calle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['profesionals.destroy', $profesional->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('profesionals.show', [$profesional->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('profesionals.edit', [$profesional->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
