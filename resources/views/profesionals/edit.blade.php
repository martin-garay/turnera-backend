@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Editar Profesional</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($profesional, ['route' => ['profesionals.update', $profesional->id], 'method' => 'patch']) !!}

            @include('profesionals.solapas')            

            <div class="card-footer">
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('profesionals.index') }}" class="btn btn-default">Cancelar</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection


@push('page_css')
<style type="text/css">

#pills-tab {    
    background-color: #eee;
    border-bottom: #0d6efd solid 3px;
}

</style>
@endpush