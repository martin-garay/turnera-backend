<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $profesional->nombre }}</p>
</div>

<!-- Apellido Field -->
<div class="col-sm-12">
    {!! Form::label('apellido', 'Apellido:') !!}
    <p>{{ $profesional->apellido }}</p>
</div>

<!-- Matricula Field -->
<div class="col-sm-12">
    {!! Form::label('matricula', 'Matrícula:') !!}
    <p>{{ $profesional->matricula }}</p>
</div>

<!-- Dni Field -->
<div class="col-sm-12">
    {!! Form::label('dni', 'Dni:') !!}
    <p>{{ $profesional->dni }}</p>
</div>

<!-- Calle Field -->
<div class="col-sm-12">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{{ $profesional->calle }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{{ $profesional->created_at->format('d/m/Y h:i')}}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{{ $profesional->updated_at->format('d/m/Y h:i')}}</p>
</div>

