<table id="profesiones"></table>

<script type="text/javascript">

var profesiones = @isset($profesiones) @json($profesiones) @else 'null' @endisset;
var dataProfesiones = @isset($profesional->profesiones) @json($profesional->profesiones()->get(['profesionales_profesiones.id_profesion'])->toArray()) @else null @endisset;

document.addEventListener("DOMContentLoaded", function () {
    // Initialize appendGrid
    var myAppendGrid = new AppendGrid({
		element: "profesiones",
		uiFramework: "bootstrap5",
		iconFramework: "bootstrapicons",
		initRows: 0,
		initData: dataProfesiones,
		iconParams: {
			baseUrl: "https://cdn.jsdelivr.net/npm/bootstrap-icons/icons/"
		},
        columns: [
        	{
	      		name: "id_profesion",
		      	display: "Profesion",
		      	type: "select",
		      	placeHolder: "--Seleccione--",
		      	ctrlOptions: profesiones
		    },
		    
    	],
    	nameFormatter: function(idPrefix, name, uniqueIndex) {	     
	        return "profesiones["+uniqueIndex+"]["+name+"]";
	    },
    	hideRowNumColumn: true,
    	hideButtons: {
        // Hide the move up and move down button on each row
	        moveUp: true,
	        moveDown: true,
	        insert: true,
	        removeLast:true
	    },
	    i18n: {
	        append: 	"Agregar",
	        removeLast: "Eliminar ultima fila",
	        insert: 	"Insertar fila"	,
	        remove: 	"Eliminar fila",
	        moveUp: 	"Mover fila hacia arriba",
			moveDown: 	"Mover fila hacia abajo",
	        rowEmpty: 	"No hay datos cargados"
	    }
    });
});
</script>