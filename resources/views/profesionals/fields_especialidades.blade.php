<table id="especialidades"></table>

<script type="text/javascript">

var especialidades = @isset($especialidades) @json($especialidades) @else 'null' @endisset;
		
var dataEspecialidades = @isset($profesional->especialidades) @json($profesional->especialidades()->get(['profesionales_especialidades.id_especialidad'])->toArray()) @else null @endisset;

document.addEventListener("DOMContentLoaded", function () {
    // Initialize appendGrid
    var myAppendGrid = new AppendGrid({
		element: "especialidades",
		uiFramework: "bootstrap5",
		iconFramework: "bootstrapicons",
		initRows: 0,
		initData: dataEspecialidades,
		iconParams: {
			baseUrl: "https://cdn.jsdelivr.net/npm/bootstrap-icons/icons/"
		},
        columns: [
        	{
	      		name: "id_especialidad",
		      	display: "Especialidad",
		      	type: "select",
		      	placeHolder: "--Seleccione--",
		      	ctrlOptions: especialidades
		    },
		    
    	],
    	nameFormatter: function(idPrefix, name, uniqueIndex) {	     
	        return "especialidades["+uniqueIndex+"]["+name+"]";
	    },
    	hideRowNumColumn: true,
    	hideButtons: {
        // Hide the move up and move down button on each row
	        moveUp: true,
	        moveDown: true,
	        insert: true,
	        removeLast:true
	    },
	    i18n: {
	        append: 	"Agregar",
	        removeLast: "Eliminar ultima fila",
	        insert: 	"Insertar fila"	,
	        remove: 	"Eliminar fila",
	        moveUp: 	"Mover fila hacia arriba",
			moveDown: 	"Mover fila hacia abajo",
	        rowEmpty: 	"No hay datos cargados"
	    }
    });
});
</script>