<li class="nav-item has-treeview">
    <a href="#"
       class="nav-link">
        <i class="fas fa-cog"></i>
        <p>Configuración</p>
        <i class="right fas fa-angle-left"></i>
    </a>
    <ul class="nav nav-treeview">
  
        <li class="nav-item">
            <a href="{{ route('rubros.index') }}"
               class="nav-link {{ Request::is('rubros*') ? 'active' : '' }}">
               <i class="fas fa-registered"></i>
                <p>Rubros</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('empresas.index') }}"
               class="nav-link {{ Request::is('empresas*') ? 'active' : '' }}">
                <i class="far fa-building"></i>
                <p>Empresas</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('profesions.index') }}"
               class="nav-link {{ Request::is('profesions*') ? 'active' : '' }}">
               <i class="fas fa-stethoscope"></i>
                <p>Profesiones</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('especialidads.index') }}"
               class="nav-link {{ Request::is('especialidads*') ? 'active' : '' }}">
                <i class="fas fa-book-medical"></i>
                <p>Especialidades</p>
            </a>
        </li>

    </ul>
</li>



<li class="nav-item">
    <a href="{{ route('profesionals.index') }}"
       class="nav-link {{ Request::is('profesionals*') ? 'active' : '' }}">
        <i class="fas fa-user-tie"></i>
        <p>Profesionales</p>
    </a>
</li>



<li class="nav-item">
    <a href="{{ route('sucursals.index') }}"
       class="nav-link {{ Request::is('sucursals*') ? 'active' : '' }}">
        <i class="fas fa-hospital-alt"></i>        
        <p>Sucursales</p>
    </a>
    
</li>


<li class="nav-item">
    <a href="{{ route('feriados.index') }}"
       class="nav-link {{ Request::is('feriados*') ? 'active' : '' }}">
        <i class="fas fa-calendar-times"></i>
        <p>Feriados</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('parametrizacionTurnos.index') }}"
       class="nav-link {{ Request::is('parametrizacionTurnos*') ? 'active' : '' }}">
        <i class="far fa-calendar-plus"></i>
        <p>Parametrizacion Turnos</p>
    </a>
</li>


<li class="nav-item d-none">
    <a href="{{ route('dias.index') }}"
       class="nav-link {{ Request::is('dias*') ? 'active' : '' }}">
        <p>Dias</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('turnos.index') }}"
       class="nav-link {{ Request::is('turnos*') ? 'active' : '' }}">
        <i class="fas fa-calendar-alt"></i>
        <p>Turnos</p>
    </a>
</li>
<!-- <i class="fas fa-calendar-check"></i> -->

<!-- <li class="nav-item">
    <a href="{{ route('tipoInstalacions.index') }}"
       class="nav-link {{ Request::is('tipoInstalacions*') ? 'active' : '' }}">
        <p>Tipos de Instalaciones</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('instalacions.index') }}"
       class="nav-link {{ Request::is('instalacions*') ? 'active' : '' }}">
        <p>Instalacions</p>
    </a>
</li> -->


<li class="nav-item">
    <a href="{{ route('reservas.index') }}"
       class="nav-link {{ Request::is('reservas*') ? 'active' : '' }}">
       <i class="fas fa-calendar-alt"></i>
        <p>Reservas</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('agenda.index') }}"
       class="nav-link {{ Request::is('agenda*') ? 'active' : '' }}">
        <i class="fas fa-calendar-alt"></i>
        <p>Agenda</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <i class="fa fa-user"></i>
        <p>Usuarios</p>
    </a>
</li>
