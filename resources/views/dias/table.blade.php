<div class="table-responsive">
    <table class="table" id="dias-table">
        <thead>
        <tr>
            <th>Nombre</th>
            <th colspan="3">Acción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dias as $dia)
            <tr>
                <td>{{ $dia->nombre }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['dias.destroy', $dia->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('dias.show', [$dia->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('dias.edit', [$dia->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
