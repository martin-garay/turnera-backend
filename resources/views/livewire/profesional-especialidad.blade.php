<div class="form-group row">
    

<div class="form-group col-sm-6">
    {!! Form::label('id_profesional', 'Profesional:') !!}

    <select wire:model="selectedProfesional" class="form-control custom-select" name="id_profesional">
        <option value="" selected>--Seleccione--</option>
        @foreach($profesionales as $profesional)
            <option value="{{ $profesional->id }}">{{ $profesional->full_name }}</option>
        @endforeach
    </select>
</div>
    

@if (!is_null($selectedProfesional))
    

    <div class="form-group col-sm-6">
        {!! Form::label('id_especialidad', 'Especialidad:') !!}

        <select wire:model="selectedEspecialidad" class="form-control custom-select" name="id_especialidad">
            <option value="" selected>--Seleccione--</option>
            
            @foreach($especialidades as $especialidad)
                <option value="{{ $especialidad->id }}">{{ $especialidad->descripcion }}</option>
            @endforeach
        
        </select>
    </div>
    
@endif

</div>