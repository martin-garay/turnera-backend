<div class="form-group row">
    
    <div class="form-group col-sm-4">
        {!! Form::label('id_especialidad', 'Especialidad:') !!}

        <select wire:model="selectedEspecialidad" class="form-control custom-select" name="id_especialidad">
            <option value="" selected>--Seleccione--</option>
            
            @foreach($especialidades as $especialidad)
                <option value="{{ $especialidad->id }}">{{ $especialidad->descripcion }}</option>
            @endforeach
        
        </select>
    </div>
    
        

@if (!is_null($selectedEspecialidad))
    

    <div class="form-group col-sm-4">
        {!! Form::label('id_profesional', 'Profesional:') !!}

        <select wire:model="selectedProfesional" class="form-control custom-select" name="id_profesional">
            <option value="" selected>--Seleccione--</option>
            @foreach($profesionales as $profesional)
                <option value="{{ $profesional->id }}">{{ $profesional->full_name }}</option>
            @endforeach
        </select>
    </div>
    
@endif

@if (!is_null($selectedProfesional))
    
    <div class="form-group col-sm-2">
        {!! Form::label('fecha', 'Fecha:') !!}

        <select wire:model="selectedFecha" class="form-control custom-select" name="fecha">
            <option value="" selected>--Seleccione--</option>
            @foreach($fechas as $fecha)
                <option value="{{ $fecha}}">{{ $fecha }}</option>
            @endforeach
        </select>
    </div>
    
@endif

@if (!is_null($selectedFecha))
    
    <div class="form-group col-sm-2">
        {!! Form::label('turno', 'Turno:') !!}

        <select wire:model="selectedTurno" class="form-control custom-select" name="id_turno">
            <option value="" selected>--Seleccione--</option>
            @foreach($turnos as $turno)
                <option value="{{ $turno->id }}">{{ $turno->hora_desde }}</option>
            @endforeach
        </select>
    </div>
    
@endif

</div>