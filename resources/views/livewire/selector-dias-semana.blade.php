
<div class="semana">
        
    @foreach($days as $day)

        <div>
            <input type="checkbox" value="1" id="{{ $day->abreviatura }}" wire:model="selectedDays.{{ $day->id }}" class="dia_semana" @if(!$editable) readonly disabled @endif>
            <label for="{{ $day->abreviatura }}" class="{{ $selectedDays[$day->id] ? 'check' : 'uncheck' }}">{{ $day->abreviatura }}</label>
        </div>

    @endforeach
        
    <input name="dias" class="form-control" id="dias" type="text" wire:model="selectedDaysID">
    
</div>


<style type="text/css">
  
    .dia_semana {
      display: none;
    }

    .semana > div > label  {
      /*color: #ccc;*/
      cursor: pointer;
    }

    .semana > div{      
        display: inline;
    }

    .check{
        color: #333 !important;
    }

    .uncheck{
        color: #ccc !important;
    }

    #dias{
        display: none;
    }
</style>