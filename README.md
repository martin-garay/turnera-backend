# Turnera

Backend Turnera

## Stack

* Laravel ^8.54
* PostgreSQL ^9.X
* PHP ^7.4
* Composer (global)

## Instalación del proyecto (Linux)

### Paso 0: Clonar el proyecto

En el directorio de instalacion

```bash
git clone https://gitlab.com/laacademia/turnera-backend.git turnera
cd turnera
```

### Paso 1: Archivo de configuración

Generar el archivo de configuracion y editarlo con los datos de la base que correspondan

```bash
cp .env.example .env
nano .env
```

Las variables a editar suelen ser las que tienen el prefijo `DB_`.

Ejemplo para un entorno de desarrollo local:

```ini
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=turnera
DB_USERNAME=postgres
DB_PASSWORD=postgres
```

### Paso 2: Instalación de dependencias

Se hace vía composer:

```bash
composer install
```

### Paso 3: Clave del proyecto

Laravel usa una clave para identificar la instalación, es necesaria generarla:

```bash
php artisan key:generate
```

### Paso 4: Generar la base de datos

(Paso opcional si se quiere tener la base local):

```bash
php artisan migrate
```

**Listo.**

## Usar el proyecto

En el directorio del proyecto clonado

```bash
php artisan serve
```

Luego dirigirse a http://127.0.0.1:8000

## Uso de la Libreria Laravel-Generator

Crear todo un CRUD para un maestro:

```bash
php artisan infyom:scaffold Rubro
```

Para entender los tipos de datos y opciones del comando, [revisar la documentación](https://infyom.com/open-source/laravelgenerator/docs/8.0/getting-started).

