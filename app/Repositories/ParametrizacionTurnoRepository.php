<?php

namespace App\Repositories;

use App\Models\ParametrizacionTurno;
use App\Repositories\BaseRepository;

/**
 * Class ParametrizacionTurnoRepository
 * @package App\Repositories
 * @version October 10, 2021, 2:37 pm UTC
*/

class ParametrizacionTurnoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha_desde',
        'fecha_hasta',
        'hora_desde',
        'hora_hasta',
        'duracion_turno',
        'sobreturnos',
        'generado',
        'id_sucursal',
        'id_profesional',
        'id_especialidad'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ParametrizacionTurno::class;
    }
}
