<?php

namespace App\Repositories;

use App\Models\TipoInstalacion;
use App\Repositories\BaseRepository;

/**
 * Class TipoInstalacionRepository
 * @package App\Repositories
 * @version November 28, 2021, 9:49 pm UTC
*/

class TipoInstalacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'activo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoInstalacion::class;
    }
}
