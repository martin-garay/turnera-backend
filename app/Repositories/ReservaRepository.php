<?php

namespace App\Repositories;

use App\Models\Reserva;
use App\Repositories\BaseRepository;

/**
 * Class ReservaRepository
 * @package App\Repositories
 * @version January 31, 2022, 9:56 pm UTC
*/

class ReservaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_usuario',
        'observaciones',
        'id_turno'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reserva::class;
    }
}
