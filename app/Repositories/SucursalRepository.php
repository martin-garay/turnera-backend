<?php

namespace App\Repositories;

use App\Models\Sucursal;
use App\Repositories\BaseRepository;

/**
 * Class SucursalRepository
 * @package App\Repositories
 * @version October 10, 2021, 11:53 am UTC
*/

class SucursalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'calle',
        'altura',
        'departamento',
        'piso',
        'id_localidad',
        'telefono_fijo',
        'telefono_celular',
        'mail',
        'id_empresa',
        'orden',
        'casa_central',
        'activo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sucursal::class;
    }
}
