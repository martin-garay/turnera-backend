<?php

namespace App\Repositories;

use App\Models\Reserva;
use App\Repositories\BaseRepository;

/**
 * Class ListadoReservasRepository
 * @package App\Repositories
 * @version February 3, 2022, 10:55 am UTC
*/

class ListadoReservasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'usuario',
        'fecha',
        'hora',
        'profesional',
        'especialidad',
        'estado'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reserva::class;
    }
}
