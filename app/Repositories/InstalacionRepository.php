<?php

namespace App\Repositories;

use App\Models\Instalacion;
use App\Repositories\BaseRepository;

/**
 * Class InstalacionRepository
 * @package App\Repositories
 * @version November 30, 2021, 4:32 pm UTC
*/

class InstalacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'valor_hora',
        'capacidad_personas',
        'observaciones',
        'id_tipo_instalacion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Instalacion::class;
    }
}
