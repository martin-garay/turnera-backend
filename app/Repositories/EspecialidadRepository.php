<?php

namespace App\Repositories;

use App\Models\Especialidad;
use App\Repositories\BaseRepository;

/**
 * Class EspecialidadRepository
 * @package App\Repositories
 * @version October 9, 2021, 2:24 pm UTC
*/

class EspecialidadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'activo',
        'id_empresa'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Especialidad::class;
    }
}
