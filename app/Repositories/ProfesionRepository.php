<?php

namespace App\Repositories;

use App\Models\Profesion;
use App\Repositories\BaseRepository;

/**
 * Class ProfesionRepository
 * @package App\Repositories
 * @version October 8, 2021, 9:39 pm UTC
*/

class ProfesionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'activo',
        'id_empresa'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Profesion::class;
    }
}
