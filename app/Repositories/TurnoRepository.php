<?php

namespace App\Repositories;

use App\Models\Turno;
use App\Repositories\BaseRepository;

/**
 * Class TurnoRepository
 * @package App\Repositories
 * @version October 11, 2021, 8:41 pm UTC
*/

class TurnoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'hora_desde',
        'hora_hasta',
        'id_estado',
        'fecha_generacion',
        'id_profesional',
        'id_especialidad',
        'id_parametrizacion',
        'sobreturnos'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Turno::class;
    }
}
