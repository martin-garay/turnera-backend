<?php

namespace App\Repositories;

use App\Models\Dia;
use App\Repositories\BaseRepository;

/**
 * Class DiaRepository
 * @package App\Repositories
 * @version October 11, 2021, 11:47 am UTC
*/

class DiaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dia::class;
    }
}
