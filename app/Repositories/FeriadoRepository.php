<?php

namespace App\Repositories;

use App\Models\Feriado;
use App\Repositories\BaseRepository;

/**
 * Class FeriadoRepository
 * @package App\Repositories
 * @version October 10, 2021, 1:33 pm UTC
*/

class FeriadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'hora_desde',
        'hora_hasta',
        'id_sucursal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feriado::class;
    }
}
