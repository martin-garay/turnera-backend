<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Feriado
 * @package App\Models
 * @version October 10, 2021, 1:33 pm UTC
 *
 * @property string $fecha
 * @property time $hora_desde
 * @property time $hora_hasta
 * @property integer $id_sucursal
 */
class Feriado extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'feriados';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'fecha',
        'descripcion',
        'hora_desde',
        'hora_hasta',
        'id_sucursal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'fecha' => 'date',
        'descripcion' => 'string',
        'hora_desde' => 'string',
        'hora_hasta' => 'string',
        'id_sucursal' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha' => 'required',
        'descripcion' => 'required|max:255',
        'hora_desde' => 'nullable|date_format:H:i',
        'hora_hasta' => 'nullable|date_format:H:i|after:hora_desde',
        'id_sucursal' => 'required'
    ];

    function getHoraDesdeAttribute(){
        return (strlen($this->attributes['hora_desde'])>5) ? substr($this->attributes['hora_desde'], 0, 5) : $this->attributes['hora_desde'];         
    }

    function getHoraHastaAttribute(){
        return (strlen($this->attributes['hora_hasta'])>5) ? substr($this->attributes['hora_hasta'], 0, 5) : $this->attributes['hora_hasta'];
    }

    function sucursal(){
        return $this->belongsTo(Sucursal::class,'id_sucursal','id');       
    }

    static function esFeriado($id_sucursal, $fecha, $hora){
        return self::where(['id_sucursal'=>$id_sucursal, 'fecha'=>$fecha])->first();
    }


}
