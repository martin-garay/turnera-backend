<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Empresa;

/**
 * Class Rubro
 * @package App\Models
 * @version October 3, 2021, 2:09 pm UTC
 *
 * @property string $descripcion
 * @property boolean $activo
 */
class Rubro extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'rubros';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'descripcion',
        'activo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'descripcion' => 'string',
        'activo' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required|max:200'
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'id', 'id_empresa');
    }
}
