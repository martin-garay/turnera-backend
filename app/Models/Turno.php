<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\ModelStates\HasStates;
use App\Models\Estados\Turno\EstadoTurno;

/**
 * Class Turno
 * @package App\Models
 * @version October 11, 2021, 8:41 pm UTC
 *
 * @property string $fecha
 * @property time $hora_desde
 * @property time $hora_hasta
 * @property string $fecha_generacion
 * @property integer $id_profesional
 * @property integer $id_especialidad
 * @property integer $id_parametrizacion
 * @property integer $sobreturnos
 * @property EstadoTurno $estado
 */
class Turno extends Model
{
    use SoftDeletes;

    use HasFactory;
    use HasStates;

    public $table = 'turnos';
    

    protected $dates = ['deleted_at','fecha', 'fecha_generacion'];

    private $weekMap = [
        0 => 'Domingo',
        1 => 'Lunes',
        2 => 'Martes',
        3 => 'Miercoles',
        4 => 'Jueves',
        5 => 'Viernes',
        6 => 'Sabado',
    ];

    public $fillable = [
        'id_sucursal',
        'fecha',
        'hora_desde',
        'hora_hasta',
        'duracion',        
        'fecha_generacion',
        'id_profesional',
        'id_especialidad',
        'id_parametrizacion',
        'sobreturnos',
        'estado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_sucursal' => 'integer',
        'fecha' => 'date',        
        'id_profesional' => 'integer',
        'id_especialidad' => 'integer',
        'id_parametrizacion' => 'integer',
        'sobreturnos' => 'integer',
        'duracion' => 'integer',
        //'fecha_generacion' => 'date',
        'estado' => EstadoTurno::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_sucursal' => 'required',
        'fecha' => 'required',
        'hora_desde' => 'required',
        'hora_hasta' => 'required',
        'duracion' => 'required|min:1',        
        'id_profesional' => 'required',
        'id_especialidad' => 'required',
        'sobreturnos' => 'max:30'
    ];

    public function getFechaTextAttribute(){        
        return $this->fecha->format('d/m/Y');
    }

    public function getFechaFormateadaAttribute(){        
        return $this->fecha->format('d/m/Y');
    }

    public function getHoraDesdeFormateadaAttribute(){        
        return substr($this->hora_desde, 0, 5);
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id');
    }

    public function profesional()
    {
        return $this->belongsTo(Profesional::class, 'id_profesional', 'id');
    }

    public function especialidad()
    {
        return $this->belongsTo(Especialidad::class, 'id_especialidad', 'id');
    }

    public function parametrizacion()
    {
        return $this->belongsTo(ParametrizacionTurno::class, 'id_parametrizacion', 'id');
    }

    public function reservas()
    {
        return $this->hasMany(Reserva::class, 'id_turno', 'id');
    }

    function getDiaSemanaDescripcionAttribute(){        
        $dayOfTheWeek = $this->fecha->dayOfWeek;
        return $this->weekMap[$dayOfTheWeek];
    }

    function getCantidadReservasAttribute(){        
        return $this->reservas()->count();
    }

    function dayOfTheWeek(){

    }
    
    //indica si el turno se solapa con otro del mismo profesional
    function haySolapamiento(){
        return false;
    }    

}
