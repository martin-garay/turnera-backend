<?php

namespace App\Models\Estados\Reserva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ausente extends EstadoReserva
{
    use HasFactory;

    public static $name = 'Ausente';
}
