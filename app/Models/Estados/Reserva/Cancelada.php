<?php

namespace App\Models\Estados\Reserva;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cancelada extends EstadoReserva
{
    use HasFactory;

    public static $name = 'Cancelada';
}
