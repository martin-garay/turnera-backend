<?php
namespace App\Models\Estados\Reserva;

use Spatie\ModelStates\State;
use Spatie\ModelStates\StateConfig;

abstract class EstadoReserva extends State
{    
    
    public static function config(): StateConfig
    {
        return parent::config()
            ->default(Reservado::class)            
            ->allowTransition(Reservado::class, Atendido::class)
            ->allowTransition(Reservado::class, Cancelada::class, TransicionReservadoACancelada::class)
            ->allowTransition(Reservado::class, Ausente::class)
            ->allowTransition(Ausente::class, Atendido::class);
    }
}