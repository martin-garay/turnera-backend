<?php
namespace App\Models\Estados\Reserva;

use Spatie\ModelStates\Transition;
use App\Models\Reserva;
use App\Events\ReservaCancelada;

class TransicionReservadoACancelada extends Transition
{
    private Reserva $reserva;    

    public function __construct(Reserva $reserva)
    {
        $this->reserva = $reserva;
    }

    public function handle(): Reserva
    {
        $this->reserva->estado = new Cancelada($this->reserva);                

        $this->reserva->save();

        ReservaCancelada::dispatch($this->reserva);

        return $this->reserva;
    }
}