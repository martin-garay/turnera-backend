<?php

namespace App\Models\Estados\Turno;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Estados\Turno\EstadoTurno;

class Inicial extends \App\Models\Estados\Turno\EstadoTurno
{
    use HasFactory;

    public static $name = 'Inicial';
}
