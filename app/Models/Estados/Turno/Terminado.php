<?php

namespace App\Models\Estados\Turno;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Terminado extends EstadoTurno
{
    use HasFactory;

    public static $name = 'Terminado';
}
