<?php
namespace App\Models\Estados\Turno;

use Spatie\ModelStates\State;
use Spatie\ModelStates\StateConfig;

abstract class EstadoTurno extends State
{
    //abstract public function color(): string;
    
    public static function config(): StateConfig
    {
        return parent::config()
            ->default(Disponible::class)            
            ->allowTransition(Disponible::class, Reservado::class)
            ->allowTransition(Disponible::class, Cancelado::class)
            ->allowTransition(Disponible::class, Terminado::class)
            ->allowTransition(Reservado::class, Disponible::class)
            ->allowTransition(Reservado::class, Cancelado::class)
            ->allowTransition(Reservado::class, Ausente::class)
            ->allowTransition(Reservado::class, Atendido::class)
            ->allowTransition(Reservado::class, Terminado::class)
            ->allowTransition(Ausente::class, Atendido::class)            
        ;
    }
}