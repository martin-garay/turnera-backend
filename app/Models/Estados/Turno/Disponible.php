<?php

namespace App\Models\Estados\Turno;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disponible extends EstadoTurno
{
    use HasFactory;

    public static $name = 'Disponible';
}
