<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class TipoInstalacion
 * @package App\Models
 * @version November 28, 2021, 9:49 pm UTC
 *
 * @property string $descripcion
 * @property boolean $activo
 * @property int $id_empresa
 */
class TipoInstalacion extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'tipo_instalaciones';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'descripcion',
        'activo',
        'id_empresa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'descripcion' => 'string',
        'activo' => 'boolean',
        'id_empresa' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required|max:100',        
    ];

    
}
