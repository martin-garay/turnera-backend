<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Dia
 * @package App\Models
 * @version October 11, 2021, 11:47 am UTC
 *
 * @property string $nombre
 */
class Dia extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'dias';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:10'
    ];

    function getAbreviaturaAttribute(){
        return substr($this->nombre, 0, 2);
    }

    
}
