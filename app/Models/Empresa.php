<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use User;


/**
 * Class Empresa
 * @package App\Models
 * @version October 3, 2021, 4:18 pm UTC
 *
 * @property string $nombre
 * @property integer $id_rubro
 * @property string $logo
 * @property string $cuit
 */
class Empresa extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'empresas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'id_rubro',
        'logo',
        'cuit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'id_rubro' => 'integer',
        'cuit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:200',
        'id_rubro' => 'required'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'id_empresa', 'id');
    }

    public function rubro()
    {
        return $this->belongsTo(Rubro::class, 'id_rubro', 'id');
    }

    public function especialidades()
    {
        return $this->hasMany(Especialidad::class, 'id_empresa', 'id');
    }

    public function profesiones()
    {
        return $this->hasMany(Profesion::class, 'id_empresa', 'id');
    }

    public function profesionales()
    {
        return $this->hasMany(Profesional::class, 'id_empresa', 'id');
    }    

    public function sucursales()
    {
        return $this->hasMany(Sucursal::class, 'id_empresa', 'id');
    }

    public function tipos_instalacion()
    {
        return $this->hasMany(TipoInstalacion::class, 'id_empresa', 'id');
    }    
    
}
