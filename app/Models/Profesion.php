<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Empresa;

/**
 * Class Profesion
 * @package App\Models
 * @version October 8, 2021, 9:39 pm UTC
 *
 * @property string $descripcion	string,200
 * @property boolean $activo
 * @property integer $id_empresa
 */
class Profesion extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'profesiones';
    

    protected $dates = ['deleted_at'];

    protected $hidden = ['pivot'];

    public $fillable = [
        'descripcion',
        'activo',
        'id_empresa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'descripcion' => 'string',
        'activo' => 'boolean',
        'id_empresa' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required|max:200',
        //'id_empresa' => 'required'
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'id', 'id_empresa');
    }

    function profesionales(){
        
    }
}
