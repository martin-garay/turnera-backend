<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Empresa;
use App\Models\Profesional;
use App\Models\Especialidad;

/**
 * Class Profesional
 * @package App\Models
 * @version October 8, 2021, 10:54 pm UTC
 *
 * @property string $nombre
 * @property string $apellido
 * @property string $matricula
 * @property integer $dni
 * @property string $calle
 */
class Profesional extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'profesionales';
    

    protected $dates = ['deleted_at'];

    protected $hidden = ['pivot'];

    public $fillable = [
        'nombre',
        'apellido',
        'matricula',
        'dni',
        'calle',
        'altura',
        'departamento',
        'id_empresa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'apellido' => 'string',
        'matricula' => 'string',
        'dni' => 'integer',
        'calle' => 'string',
        'altura' => 'integer',
        'departamento' => 'string',
        'id_empresa' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:255',
        'apellido' => 'required|max:255',
        'matricula' => 'max:255',
        'dni' => 'required|numeric|min:5000000|max:80000000',
        'calle' => 'max:255',
        'altura' => 'nullable|numeric|max:8000000',
        'departamento' => 'max:20',
    ];

    public function getFullNameAttribute()
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'id', 'id_empresa');
    }

    public function profesiones()
    {
        return $this->belongsToMany(Profesion::class,'profesionales_profesiones','id_profesional','id_profesion');
    }

    public function especialidades()
    {
        return $this->belongsToMany(Especialidad::class,'profesionales_especialidades','id_profesional','id_especialidad');
    }

}
