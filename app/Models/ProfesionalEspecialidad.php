<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfesionalEspecialidad extends Model
{
    use HasFactory;

    public $table = 'profesionales_especialidades';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_profesional',
        'id_especialidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_profesional' => 'integer',
        'id_especialidad' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_profesional' => 'required',
        'id_especialidad' => 'required'
    ];
    
}
