<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfesionalProfesion extends Model
{
    use HasFactory;

    public $table = 'profesionales_profesiones';
    

    protected $dates = ['deleted_at'];

    protected $hidden = ['pivot'];

    public $fillable = [
        'id_profesional',
        'id_profesion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_profesional' => 'integer',
        'id_profesion' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_profesional' => 'required',
        'id_profesion' => 'required'
    ];
}
