<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Sucursal
 * @package App\Models
 * @version October 10, 2021, 11:53 am UTC
 *
 * @property string $nombre
 * @property string $calle
 * @property integer $altura
 * @property string $departamento
 * @property integer $piso
 * @property integer $id_localidad
 * @property string $telefono_fijo
 * @property string $telefono_celular
 * @property string $mail
 * @property integer $id_empresa
 * @property integer $orden
 * @property boolean $casa_central
 */
class Sucursal extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'sucursales';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'calle',
        'altura',
        'departamento',
        'piso',
        'id_localidad',
        'telefono_fijo',
        'telefono_celular',
        'mail',
        'id_empresa',
        'orden',
        'casa_central',
        'activo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'calle' => 'string',
        'altura' => 'integer',
        'departamento' => 'string',
        'piso' => 'integer',
        'id_localidad' => 'integer',
        'telefono_fijo' => 'string',
        'telefono_celular' => 'string',
        'mail' => 'string',
        'id_empresa' => 'integer',
        'orden' => 'integer',
        'casa_central' => 'boolean',
        'activo' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:255',
        'calle' => 'required|max:255',
        'altura' => 'required|integer',
        'departamento' => 'nullable|max:20',
        'piso' => 'nullable|integer|max:100',
        //'id_localidad' => 'required',
        'telefono_fijo' => 'nullable|max:100',
        'telefono_celular' => 'nullable|max:100',
        'mail' => 'nullable|email',        
    ];

    function empresa()
    {
        return $this->belongsTo(Empresa::class,'id_empresa','id');
    }

    function feriados()
    {
        return $this->hasMany(Feriado::class,'id_sucursal','id');
    }    

}
