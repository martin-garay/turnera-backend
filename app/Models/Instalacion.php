<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Instalacion
 * @package App\Models
 * @version November 30, 2021, 4:32 pm UTC
 *
 * @property string $descripcion
 * @property number $valor_hora
 * @property integer $capacidad_personas
 * @property string $observaciones
 * @property integer $id_tipo_instalacion
 */
class Instalacion extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'instalaciones';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'descripcion',
        'valor_hora',
        'capacidad_personas',
        'observaciones',
        'id_tipo_instalacion',
        'id_sucursal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'descripcion' => 'string',
        'valor_hora' => 'float',
        'capacidad_personas' => 'integer',
        'observaciones' => 'string',
        'id_tipo_instalacion' => 'integer',
        'id_sucursal' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required',
        'capacidad_personas' => 'min:0',
        'id_tipo_instalacion' => 'required',
        'id_sucursal' => 'required'
    ];

    public function tipo_instalacion()
    {
        return $this->belongsTo(TipoInstalacion::class, 'id_tipo_instalacion', 'id');
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id');
    }
    
}
