<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Empresa;

/**
 * Class Especialidad
 * @package App\Models
 * @version October 9, 2021, 2:24 pm UTC
 *
 * @property string $descripcion
 * @property boolean $activo
 * @property integer $id_empresa
 */
class Especialidad extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'especialidades';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'descripcion',
        'activo',
        'id_empresa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'descripcion' => 'string',
        'activo' => 'boolean',
        'id_empresa' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required|max:200'
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'id', 'id_empresa');
    }

    public function profesionales()
    {
        return $this->belongsToMany(Profesional::class,'profesionales_especialidades','id_especialidad','id_profesional');
    }


}
