<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\ModelStates\HasStates;
use App\Models\Estados\Reserva\EstadoReserva;
/**
 * Class Reserva
 * @package App\Models
 * @version January 31, 2022, 9:56 pm UTC
 *
 * @property integer $id_usuario
 * @property string $observaciones
 * @property integer $id_turno
 */
class Reserva extends Model
{
    use SoftDeletes;

    use HasFactory;
    use HasStates;

    public $table = 'reservas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_usuario',
        'observaciones',
        'id_turno',
        'estado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_usuario' => 'integer',
        'observaciones' => 'string',
        'id_turno' => 'integer',
        'estado' => EstadoReserva::class,
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_usuario' => 'required',
        'id_turno' => 'required'
    ];

    function turno()
    {
        return $this->hasOne(Turno::class,'id','id_turno');
    }
    
    function usuario()
    {
        return $this->hasOne(User::class,'id','id_usuario');
    }
}
