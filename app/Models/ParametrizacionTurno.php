<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ParametrizacionTurno
 * @package App\Models
 * @version October 10, 2021, 2:37 pm UTC
 *
 * @property string $fecha_desde
 * @property string $fecha_hasta
 * @property time $hora_desde
 * @property time $hora_hasta
 * @property integer $duracion_turno
 * @property integer $sobreturnos
 * @property boolean $generado
 * @property integer $id_sucursal
 * @property integer $id_profesional
 * @property integer $id_especialidad
 */
class ParametrizacionTurno extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'parametrizacion_turnos';
    

    protected $dates = ['deleted_at', 'fecha_desde', 'fecha_hasta'];



    public $fillable = [
        'fecha_desde',
        'fecha_hasta',
        'hora_desde',
        'hora_hasta',
        'duracion_turno',
        'sobreturnos',        
        'id_sucursal',
        'id_profesional',
        'id_especialidad',
        'dias',
        'generado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'fecha_desde' => 'date',
        'fecha_hasta' => 'date',
        'duracion_turno' => 'integer',
        'sobreturnos' => 'integer',        
        'id_sucursal' => 'integer',
        'id_profesional' => 'integer',
        'id_especialidad' => 'integer',
        'dias' => 'string',
        'generado' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha_desde' => 'required',
        'fecha_hasta' => 'required',
        'hora_desde' => 'required',
        'hora_hasta' => 'required',
        'duracion_turno' => 'required',        
        'id_sucursal' => 'required',
        'id_profesional' => 'required',
        'id_especialidad' => 'required',
        'dias' => 'required'
    ];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id');
    }

    public function profesional()
    {
        return $this->belongsTo(Profesional::class, 'id_profesional', 'id');
    }

    public function especialidad()
    {
        return $this->belongsTo(Especialidad::class, 'id_especialidad', 'id');
    }

    function dias(){

        $dias = explode(',', $this->dias);
        $collection = new \Illuminate\Database\Eloquent\Collection;
        
        if(!empty($dias)){
            foreach ($dias as $key => $id_dia) {
                $dia = Dia::find($id_dia);
                if($dia)
                    $collection->push($dia);
            }
        }
        
        return $collection;
    }
    
}
