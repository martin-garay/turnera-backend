<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTipoInstalacionRequest;
use App\Http\Requests\UpdateTipoInstalacionRequest;
use App\Repositories\TipoInstalacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;

class TipoInstalacionController extends AppBaseController
{
    /** @var  TipoInstalacionRepository */
    private $tipoInstalacionRepository;

    public function __construct(TipoInstalacionRepository $tipoInstalacionRepo)
    {
        $this->tipoInstalacionRepository = $tipoInstalacionRepo;
    }

    /**
     * Display a listing of the TipoInstalacion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipoInstalacions = $this->tipoInstalacionRepository->all();

        return view('tipo_instalacions.index')
            ->with('tipoInstalacions', $tipoInstalacions);
    }

    /**
     * Show the form for creating a new TipoInstalacion.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_instalacions.create');
    }

    /**
     * Store a newly created TipoInstalacion in storage.
     *
     * @param CreateTipoInstalacionRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoInstalacionRequest $request)
    {
        $input = $request->all();

        $user=Auth::user();

        if (empty($user->id_empresa)) {
            Flash::error('El usuario no tiene empresa definida');

            return redirect(route('tipoInstalacions.index'));
        }

        $input['id_empresa'] = $user->id_empresa;        

        $tipoInstalacion = $this->tipoInstalacionRepository->create($input);

        Flash::success('Tipo de Instalación guardado de forma correcta.');

        return redirect(route('tipoInstalacions.index'));
    }

    /**
     * Display the specified TipoInstalacion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoInstalacion = $this->tipoInstalacionRepository->find($id);

        if (empty($tipoInstalacion)) {
            Flash::error('Tipo de Instalación no encontrado');

            return redirect(route('tipoInstalacions.index'));
        }

        return view('tipo_instalacions.show')->with('tipoInstalacion', $tipoInstalacion);
    }

    /**
     * Show the form for editing the specified TipoInstalacion.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoInstalacion = $this->tipoInstalacionRepository->find($id);

        if (empty($tipoInstalacion)) {
            Flash::error('Tipo de Instalación no encontrado');

            return redirect(route('tipoInstalacions.index'));
        }

        return view('tipo_instalacions.edit')->with('tipoInstalacion', $tipoInstalacion);
    }

    /**
     * Update the specified TipoInstalacion in storage.
     *
     * @param int $id
     * @param UpdateTipoInstalacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoInstalacionRequest $request)
    {
        $tipoInstalacion = $this->tipoInstalacionRepository->find($id);

        if (empty($tipoInstalacion)) {
            Flash::error('Tipo de Instalación no encontrado');

            return redirect(route('tipoInstalacions.index'));
        }

        $data = $request->all();

        //$data['activo'] = isset($data['activo']);   //si no esta checheado no me lo manda en el request

        $tipoInstalacion = $this->tipoInstalacionRepository->update($data, $id);

        Flash::success('Tipo de Instalación actualizado de forma correcta.');

        return redirect(route('tipoInstalacions.index'));
    }

    /**
     * Remove the specified TipoInstalacion from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoInstalacion = $this->tipoInstalacionRepository->find($id);

        if (empty($tipoInstalacion)) {
            Flash::error('Tipo de Instalación no encontrado');

            return redirect(route('tipoInstalacions.index'));
        }

        $this->tipoInstalacionRepository->delete($id);

        Flash::success('Tipo Instalacion eliminado de forma correcta.');

        return redirect(route('tipoInstalacions.index'));
    }
}
