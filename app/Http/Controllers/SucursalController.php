<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSucursalRequest;
use App\Http\Requests\UpdateSucursalRequest;
use App\Repositories\SucursalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;


class SucursalController extends AppBaseController
{
    /** @var  SucursalRepository */
    private $sucursalRepository;

    public function __construct(SucursalRepository $sucursalRepo)
    {
        $this->sucursalRepository = $sucursalRepo;
    }

    /**
     * Display a listing of the Sucursal.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //$sucursals = $this->sucursalRepository->all();
        $sucursales = Auth::user()->empresa->sucursales->all();

        return view('sucursals.index')
            ->with('sucursals', $sucursales);
    }

    /**
     * Show the form for creating a new Sucursal.
     *
     * @return Response
     */
    public function create()
    {
        return view('sucursals.create');
    }

    /**
     * Store a newly created Sucursal in storage.
     *
     * @param CreateSucursalRequest $request
     *
     * @return Response
     */
    public function store(CreateSucursalRequest $request)
    {
        $input = $request->all();

        $user=Auth::user();
        
        if (empty($user->id_empresa)) {
            Flash::error('El usuario no tiene empresa definida');

            return redirect(route('profesions.index'));
        }

        $input['id_empresa'] = $user->id_empresa;

        $sucursal = $this->sucursalRepository->create($input);

        Flash::success('Sucursal guardado de forma correcta.');

        return redirect(route('sucursals.index'));
    }

    /**
     * Display the specified Sucursal.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sucursal = $this->sucursalRepository->find($id);

        if (empty($sucursal)) {
            Flash::error('Sucursal no encontrado');

            return redirect(route('sucursals.index'));
        }

        return view('sucursals.show')->with('sucursal', $sucursal);
    }

    /**
     * Show the form for editing the specified Sucursal.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sucursal = $this->sucursalRepository->find($id);

        if (empty($sucursal)) {
            Flash::error('Sucursal no encontrado');

            return redirect(route('sucursals.index'));
        }

        return view('sucursals.edit')->with('sucursal', $sucursal);
    }

    /**
     * Update the specified Sucursal in storage.
     *
     * @param int $id
     * @param UpdateSucursalRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSucursalRequest $request)
    {
        $sucursal = $this->sucursalRepository->find($id);

        if (empty($sucursal)) {
            Flash::error('Sucursal no encontrado');

            return redirect(route('sucursals.index'));
        }

        $sucursal = $this->sucursalRepository->update($request->all(), $id);

        Flash::success('Sucursal actualizado de forma correcta.');

        return redirect(route('sucursals.index'));
    }

    /**
     * Remove the specified Sucursal from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sucursal = $this->sucursalRepository->find($id);

        if (empty($sucursal)) {
            Flash::error('Sucursal no encontrado');

            return redirect(route('sucursals.index'));
        }

        $this->sucursalRepository->delete($id);

        Flash::success('Sucursal borrado de forma correcta.');

        return redirect(route('sucursals.index'));
    }
}
