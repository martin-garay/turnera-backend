<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDiaRequest;
use App\Http\Requests\UpdateDiaRequest;
use App\Repositories\DiaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DiaController extends AppBaseController
{
    /** @var  DiaRepository */
    private $diaRepository;

    public function __construct(DiaRepository $diaRepo)
    {
        $this->diaRepository = $diaRepo;
    }

    /**
     * Display a listing of the Dia.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $dias = $this->diaRepository->all();

        return view('dias.index')
            ->with('dias', $dias);
    }

    /**
     * Show the form for creating a new Dia.
     *
     * @return Response
     */
    public function create()
    {
        return view('dias.create');
    }

    /**
     * Store a newly created Dia in storage.
     *
     * @param CreateDiaRequest $request
     *
     * @return Response
     */
    public function store(CreateDiaRequest $request)
    {
        $input = $request->all();

        $dia = $this->diaRepository->create($input);

        Flash::success('Dia guardado de forma correcta.');

        return redirect(route('dias.index'));
    }

    /**
     * Display the specified Dia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dia = $this->diaRepository->find($id);

        if (empty($dia)) {
            Flash::error('Dia no encontrado');

            return redirect(route('dias.index'));
        }

        return view('dias.show')->with('dia', $dia);
    }

    /**
     * Show the form for editing the specified Dia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dia = $this->diaRepository->find($id);

        if (empty($dia)) {
            Flash::error('Dia no encontrado');

            return redirect(route('dias.index'));
        }

        return view('dias.edit')->with('dia', $dia);
    }

    /**
     * Update the specified Dia in storage.
     *
     * @param int $id
     * @param UpdateDiaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDiaRequest $request)
    {
        $dia = $this->diaRepository->find($id);

        if (empty($dia)) {
            Flash::error('Dia no encontrado');

            return redirect(route('dias.index'));
        }

        $dia = $this->diaRepository->update($request->all(), $id);

        Flash::success('Dia actualizado de forma correcta.');

        return redirect(route('dias.index'));
    }

    /**
     * Remove the specified Dia from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dia = $this->diaRepository->find($id);

        if (empty($dia)) {
            Flash::error('Dia no encontrado');

            return redirect(route('dias.index'));
        }

        $this->diaRepository->delete($id);

        Flash::success('Dia borrado de forma correcta.');

        return redirect(route('dias.index'));
    }
}
