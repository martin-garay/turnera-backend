<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReservaRequest;
use App\Http\Requests\UpdateReservaRequest;
use App\Repositories\ReservaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Turno;
use App\Models\Estados\Turno\Disponible;
use App\Models\Estados\Turno\Reservado;
use App\Models\Estados\Reserva\Cancelada;
use App\Events\ReservaCreada;

class ReservaController extends AppBaseController
{
    /** @var  ReservaRepository */
    private $reservaRepository;

    public function __construct(ReservaRepository $reservaRepo)
    {
        $this->reservaRepository = $reservaRepo;
    }

    /**
     * Display a listing of the Reserva.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $reservas = $this->reservaRepository->all();

        return view('reservas.index')
            ->with('reservas', $reservas);
    }

    /**
     * Show the form for creating a new Reserva.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();

        $usuarios = DB::table('users')
            ->where('id_empresa',$user->id_empresa)
            ->pluck('name','id');        

        return view('reservas.create')            
            ->with('usuarios',$usuarios);
    }

    /**
     * Store a newly created Reserva in storage.
     *
     * @param CreateReservaRequest $request
     *
     * @return Response
     */
    public function store(CreateReservaRequest $request)
    {
        $input = $request->all();

        //aca tengo que controlar que este disponible
        $reserva = $this->reservaRepository->create($input);

        if($reserva->turno->cantidad_reservas >= $reserva->turno->sobreturnos)
            $reserva->turno->estado->transitionTo(Reservado::class, 'Error al cambiar el estado del Turno');

        ReservaCreada::dispatch($reserva);

        Flash::success('Reserva guardada de forma correcta.');

        return redirect(route('reservas.index'));
    }

    /**
     * Display the specified Reserva.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva no encontrada');

            return redirect(route('reservas.index'));
        }

        return view('reservas.show')->with('reserva', $reserva);
    }

    /**
     * Show the form for editing the specified Reserva.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva no encontrada');

            return redirect(route('reservas.index'));
        }

        $user=Auth::user();

        $usuarios = DB::table('users')
            ->where('id_empresa',$user->id_empresa)
            ->pluck('name','id');               
        
        return view('reservas.edit')
            ->with('reserva', $reserva)            
            ->with('usuarios',$usuarios);
    }

    /**
     * Update the specified Reserva in storage.
     *
     * @param int $id
     * @param UpdateReservaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReservaRequest $request)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva no encontrada');

            return redirect(route('reservas.index'));
        }

        $input = $request->all();
        
        $cambio_turno = $reserva->id_turno!=$input['id_turno'];
        //si cambio el turno libero el anterior y reservo el actual
        if($cambio_turno){            
            $reserva->turno->estado->transitionTo(Disponible::class, 'Error al liberar el Turno');        
        }


        $reserva = $this->reservaRepository->update($request->all(), $id);

        if($cambio_turno)
            $reserva->turno->estado->transitionTo(Reservado::class, 'Error al reservar el Turno');

        Flash::success('Reserva actualizada de forma correcta.');

        return redirect(route('reservas.index'));
    }

    /**
     * Remove the specified Reserva from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva no encontrada');

            return redirect(route('reservas.index'));
        }

        $reserva->turno->estado->transitionTo(Disponible::class, 'Error al liberar el Turno');

        $this->reservaRepository->delete($id);

        Flash::success('Reserva borrada correctamente.');

        return redirect(route('reservas.index'));
    }

    /**
     * Cancelar Reserva
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function cancelar($id)
    {
        $reserva = $this->reservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Reserva no encontrada');

            return redirect(route('reservas.index'));
        }

        $reserva->estado->transitionTo(Cancelada::class, 'Error al cancelar la reserva');        

        Flash::success('Reserva cancelada correctamente.');

        return redirect(route('reservas.index'));
    }
}
