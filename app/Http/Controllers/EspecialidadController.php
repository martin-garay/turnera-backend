<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEspecialidadRequest;
use App\Http\Requests\UpdateEspecialidadRequest;
use App\Repositories\EspecialidadRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;

class EspecialidadController extends AppBaseController
{
    /** @var  EspecialidadRepository */
    private $especialidadRepository;

    public function __construct(EspecialidadRepository $especialidadRepo)
    {
        $this->especialidadRepository = $especialidadRepo;
    }

    /**
     * Display a listing of the Especialidad.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user=Auth::user();

        $especialidads = $this->especialidadRepository->all(['id_empresa'=>$user->id_empresa]);

        return view('especialidads.index')
            ->with('especialidads', $especialidads);
    }

    /**
     * Show the form for creating a new Especialidad.
     *
     * @return Response
     */
    public function create()
    {
        return view('especialidads.create');
    }

    /**
     * Store a newly created Especialidad in storage.
     *
     * @param CreateEspecialidadRequest $request
     *
     * @return Response
     */
    public function store(CreateEspecialidadRequest $request)
    {
        $input = $request->all();

        $user=Auth::user();

        if (empty($user->id_empresa)) {
            Flash::error('El usuario no tiene empresa definida');

            return redirect(route('especialidads.index'));
        }

        $input['id_empresa'] = $user->id_empresa;

        $especialidad = $this->especialidadRepository->create($input);

        Flash::success('Especialidad guardado de forma correcta.');

        return redirect(route('especialidads.index'));
    }

    /**
     * Display the specified Especialidad.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $especialidad = $this->especialidadRepository->find($id);

        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrado');

            return redirect(route('especialidads.index'));
        }

        return view('especialidads.show')->with('especialidad', $especialidad);
    }

    /**
     * Show the form for editing the specified Especialidad.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $especialidad = $this->especialidadRepository->find($id);

        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrado');

            return redirect(route('especialidads.index'));
        }

        return view('especialidads.edit')->with('especialidad', $especialidad);
    }

    /**
     * Update the specified Especialidad in storage.
     *
     * @param int $id
     * @param UpdateEspecialidadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEspecialidadRequest $request)
    {
        $especialidad = $this->especialidadRepository->find($id);

        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrado');

            return redirect(route('especialidads.index'));
        }

        $especialidad = $this->especialidadRepository->update($request->all(), $id);

        Flash::success('Especialidad actualizado de forma correcta.');

        return redirect(route('especialidads.index'));
    }

    /**
     * Remove the specified Especialidad from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $especialidad = $this->especialidadRepository->find($id);

        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrado');

            return redirect(route('especialidads.index'));
        }

        $this->especialidadRepository->delete($id);

        Flash::success('Especialidad borrado de forma correcta.');

        return redirect(route('especialidads.index'));
    }
}
