<?php

namespace App\Http\Controllers;

use App\DataTables\AgendaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReservaRequest;
use App\Http\Requests\UpdateReservaRequest;
use App\Repositories\ReservaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AgendaController extends AppBaseController
{
    /** @var  ReservaRepository */
    private $ReservaRepository;

    public function __construct(ReservaRepository $reservasRepo)
    {
        $this->ReservaRepository = $reservasRepo;
    }

    /**
     * Display a listing of the agenda.
     *
     * @param reservaDataTable $reservaDataTable
     * @return Response
     */
    public function index(AgendaDataTable $agendaDataTable)
    {
        return $agendaDataTable->render('agenda.index');
    }

    /**
     * Display the specified agenda.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $reserva = $this->ReservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Agenda no encontrada');

            return redirect(route('agenda.index'));
        }

        return view('agenda.show')->with('reserva', $reserva);
    }

    /**
     * Update the specified Agenda in storage.
     *
     * @param  int              $id     
     *
     * @return Response
     */
    public function ausente($id)
    {        

        $reserva = $this->ReservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Agenda no encontrada');

            return redirect(route('agenda.index'));
        }

        $reserva = $this->ReservaRepository->update($request->all(), $id);

        Flash::success('Agenda actualizada correctamente.');

        return redirect(route('agenda.index'));
    }

    /**
     * Update the specified Agenda in storage.
     *
     * @param  int              $id    
     * 
     * @return Response
     */
    public function presente($id)
    {        
        $reserva = $this->ReservaRepository->find($id);

        if (empty($reserva)) {
            Flash::error('Agenda no encontrada');

            return redirect(route('agenda.index'));
        }

        $reserva = $this->ReservaRepository->update($request->all(), $id);

        Flash::success('Agenda actualizada correctamente.');

        return redirect(route('agenda.index'));
    }

}
