<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateParametrizacionTurnoRequest;
use App\Http\Requests\UpdateParametrizacionTurnoRequest;
use App\Repositories\ParametrizacionTurnoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use App\Models\Turno;
use App\Models\Feriado;

class ParametrizacionTurnoController extends AppBaseController
{
    /** @var  ParametrizacionTurnoRepository */
    private $parametrizacionTurnoRepository;

    public function __construct(ParametrizacionTurnoRepository $parametrizacionTurnoRepo)
    {
        $this->parametrizacionTurnoRepository = $parametrizacionTurnoRepo;
    }

    /**
     * Display a listing of the ParametrizacionTurno.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $parametrizacionTurnos = $this->parametrizacionTurnoRepository->all();

        return view('parametrizacion_turnos.index')
            ->with('parametrizacionTurnos', $parametrizacionTurnos);
    }

    /**
     * Show the form for creating a new ParametrizacionTurno.
     *
     * @return Response
     */
    public function create()
    {
        $user=Auth::user();

        $sucursales = $user->empresa->sucursales->pluck('nombre','id');
        

        return view('parametrizacion_turnos.create')
            ->with('sucursales',$sucursales);
    }

    /**
     * Store a newly created ParametrizacionTurno in storage.
     *
     * @param CreateParametrizacionTurnoRequest $request
     *
     * @return Response
     */
    public function store(CreateParametrizacionTurnoRequest $request)
    {
        $input = $request->all();

        $input['sobreturnos'] = 0;
        $parametrizacionTurno = $this->parametrizacionTurnoRepository->create($input);

        Flash::success('Parametrizacion Turno guardado de forma correcta.');

        return redirect(route('parametrizacionTurnos.index'));
    }

    /**
     * Display the specified ParametrizacionTurno.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $parametrizacionTurno = $this->parametrizacionTurnoRepository->find($id);

        if (empty($parametrizacionTurno)) {
            Flash::error('Parametrizacion Turno no encontrado');

            return redirect(route('parametrizacionTurnos.index'));
        }

        return view('parametrizacion_turnos.show')->with('parametrizacionTurno', $parametrizacionTurno);
    }

    /**
     * Show the form for editing the specified ParametrizacionTurno.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $parametrizacionTurno = $this->parametrizacionTurnoRepository->find($id);

        $user=Auth::user();

        $sucursales = $user->empresa->sucursales->pluck('nombre','id');

        if (empty($parametrizacionTurno)) {
            Flash::error('Parametrizacion Turno no encontrado');

            return redirect(route('parametrizacionTurnos.index'));
        }

        return view('parametrizacion_turnos.edit')
            ->with('parametrizacionTurno', $parametrizacionTurno)
            ->with('sucursales', $sucursales);
    }

    /**
     * Update the specified ParametrizacionTurno in storage.
     *
     * @param int $id
     * @param UpdateParametrizacionTurnoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateParametrizacionTurnoRequest $request)
    {
        $parametrizacionTurno = $this->parametrizacionTurnoRepository->find($id);

        if (empty($parametrizacionTurno)) {
            Flash::error('Parametrizacion Turno no encontrado');

            return redirect(route('parametrizacionTurnos.index'));
        }

        if ($parametrizacionTurno->generado) {
            Flash::error('No se puede editar una parametrización generada');

            return redirect(route('parametrizacionTurnos.index'));
        }

        $input = $request->all();
        $input['sobreturnos'] = 0;

        $parametrizacionTurno = $this->parametrizacionTurnoRepository->update($input, $id);

        Flash::success('Parametrizacion Turno actualizado de forma correcta.');

        return redirect(route('parametrizacionTurnos.index'));
    }

    /**
     * Remove the specified ParametrizacionTurno from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $parametrizacionTurno = $this->parametrizacionTurnoRepository->find($id);

        if (empty($parametrizacionTurno)) {
            Flash::error('Parametrizacion Turno no encontrado');

            return redirect(route('parametrizacionTurnos.index'));
        }

        if ($parametrizacionTurno->generado) {
            Flash::error('No se puede eliminar una parametrización generada');

            return redirect(route('parametrizacionTurnos.index'));
        }

        $this->parametrizacionTurnoRepository->delete($id);

        Flash::success('Parametrizacion Turno borrado de forma correcta.');

        return redirect(route('parametrizacionTurnos.index'));
    }

     /**
     * Show the turnos for the specified ParametrizacionTurno.
     * ref: https://stackoverflow.com/a/65422833
     * ref: https://stackoverflow.com/a/50854594
     *
     * @param int $id
     *
     * @return Response
     */
    public function visualizarTurnos($id)
    {
        $param = $this->parametrizacionTurnoRepository->find($id);

        if (empty($param)) {
            Flash::error('Parametrizacion Turno no encontrado');

            return redirect(route('parametrizacionTurnos.index'));
        }

        if($param->generado){
            Flash::error('Ya se generarón los Turnos de esa parametrización');

            return redirect(route('parametrizacionTurnos.index'));
        }

        $dias = CarbonPeriod::create($param->fecha_desde, $param->fecha_hasta);
        $horarios = new CarbonPeriod($param->hora_desde, $param->duracion_turno.' minutes', $param->hora_hasta); // for create use 24 hours format later change format 
        $turnos = new \Illuminate\Database\Eloquent\Collection;
        $feriados = new \Illuminate\Database\Eloquent\Collection;
        $solapados = new \Illuminate\Database\Eloquent\Collection;
        //dd($horarios);
        $dias_semana = explode(',', $param->dias);

        foreach ($dias as $fecha) {
        
            if( in_array($this->dayOfWeek($fecha),$dias_semana) ){

                foreach($horarios as $hora){
                    
                    $hora_hasta = $hora->toImmutable()->addMinutes($param->duracion_turno); //inmutable para que no me modifique el objeto $hora
                    $ultimo_turno = Carbon::createFromFormat('Y-m-d H:i:s', $fecha->format('Y-m-d') . ' '.$param->hora_hasta );

                    if( $hora_hasta->lessThanOrEqualTo($ultimo_turno) ){        //si el turno termina mas alla de la hora hasta no lo agrego
                        
                        $turno = new Turno([
                            'fecha'             => $fecha->format('Y-m-d'),
                            'hora_desde'        => $hora->format("h:i"),
                            'hora_hasta'        => $hora_hasta->format("h:i"),                        
                            'duracion'          => $param->duracion_turno,                            
                            'id_sucursal'       => $param->id_sucursal,
                            'id_profesional'    => $param->id_profesional,
                            'id_especialidad'   => $param->id_especialidad,
                            'sobreturnos'       => $param->sobreturnos,
                            'id_parametrizacion' => $param->id,
                            'test' => 'test'
                        ]);
                        
                        //si es feriado el dia lo excluyo
                        if(Feriado::esFeriado($param->id_sucursal, $fecha, $hora)){
                            $feriados->push($turno);
                        }else{
                            if($turno->haySolapamiento())
                                $solapados->push($turno);
                            else
                                $turnos->push($turno);
                        }

                    }
                }
            }
        }

        return view('parametrizacion_turnos.visualizar_turnos')
            ->with('parametrizacionTurno', $param)
            ->with('turnos', $turnos)
            ->with('feriados', $feriados)
            ->with('solapados', $solapados);
    }

    /**
     * Show the form for editing the specified ParametrizacionTurno.
     *
     * @param int $id
     *
     * @return Response
     */
    public function generarTurnos($id)
    {
        $param = $this->parametrizacionTurnoRepository->find($id);

        $user=Auth::user();

        $sucursales = $user->empresa->sucursales->pluck('nombre','id');
        
        if (empty($param)) {
            Flash::error('Parametrizacion Turno no encontrado');

            return redirect(route('parametrizacionTurnos.index'));
        }

        $dias = CarbonPeriod::create($param->fecha_desde, $param->fecha_hasta);
        $horarios = new CarbonPeriod($param->hora_desde, $param->duracion_turno.' minutes', $param->hora_hasta); // for create use 24 hours format later change format 
     
        $dias_semana = explode(',', $param->dias);

        foreach ($dias as $fecha) {
        
            if( in_array($this->dayOfWeek($fecha),$dias_semana) ){

                foreach($horarios as $hora){
                    
                    $hora_hasta = $hora->toImmutable()->addMinutes($param->duracion_turno); //inmutable para que no me modifique el objeto $hora
                    $ultimo_turno = Carbon::createFromFormat('Y-m-d H:i:s', $fecha->format('Y-m-d') . ' '.$param->hora_hasta );

                    if( $hora_hasta->lessThanOrEqualTo($ultimo_turno) ){        //si el turno termina mas alla de la hora hasta no lo agrego
                        
                        $turno = new Turno([
                            'fecha'             => $fecha->format('Y-m-d'),
                            'hora_desde'        => $hora->format("h:i"),
                            'hora_hasta'        => $hora_hasta->format("h:i"),                        
                            'duracion'          => $param->duracion_turno,                            
                            'id_sucursal'       => $param->id_sucursal,
                            'id_profesional'    => $param->id_profesional,
                            'id_especialidad'   => $param->id_especialidad,
                            'sobreturnos'       => $param->sobreturnos,
                            'id_parametrizacion' => $param->id,
                            'test' => 'test'
                        ]);
                                                
                        if(!Feriado::esFeriado($param->id_sucursal, $fecha, $hora) && !$turno->haySolapamiento())                            
                            $turno->save();                        

                    }
                }
            }
        }

        $param->generado = true; //paso a Generado. TODO: ver si usamos el patron de estados o creamos una tabla de estados para la param
        $param->save();


        Flash::success('Generación de Turnos de forma correcta. Parametrizacion #'.$id);

        return redirect(route('parametrizacionTurnos.index'));
    }

    function dayOfWeek($fecha){
        $weekMap = [
            0 => 7,  //SU
            1 => 1,  //MO
            2 => 2,  //TU
            3 => 3,  //WE
            4 => 4,  //TH
            5 => 5,  //FR
            6 => 6,  //SA
        ];
        $dayOfTheWeek = $fecha->dayOfWeek;
        return $weekMap[$dayOfTheWeek];
    }
}
