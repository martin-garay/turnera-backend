<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfesionalRequest;
use App\Http\Requests\UpdateProfesionalRequest;
use App\Repositories\ProfesionalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Profesion;
use App\Models\Especialidad;
use App\Models\ProfesionalProfesion;
use App\Models\ProfesionalEspecialidad;

class ProfesionalController extends AppBaseController
{
    /** @var  ProfesionalRepository */
    private $profesionalRepository;

    public function __construct(ProfesionalRepository $profesionalRepo)
    {
        $this->profesionalRepository = $profesionalRepo;
    }

    /**
     * Display a listing of the Profesional.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user=Auth::user();        

        $profesionals = $user->empresa->profesionales;

        return view('profesionals.index')
            ->with('profesionals', $profesionals);
    }

    /**
     * Show the form for creating a new Profesional.
     *
     * @return Response
     */
    public function create()
    {
        $user=Auth::user();

        $profesiones = $localidades = DB::table('profesiones')
            ->where('id_empresa',$user->id_empresa)
            ->pluck('descripcion');

        $especialidades = Especialidad::where(['activo'=>true, 'id_empresa'=>$user->id_empresa])
            ->orderBy('descripcion')
            ->pluck('descripcion', 'id');

        return view('profesionals.create')
            ->with('profesiones',$profesiones)
            ->with('especialidades',$especialidades);
    }

    /**
     * Store a newly created Profesional in storage.
     *
     * @param CreateProfesionalRequest $request
     *
     * @return Response
     */
    public function store(CreateProfesionalRequest $request)
    {
        $input = $request->all();

        $user=Auth::user();

        if (empty($user->id_empresa)) {
            Flash::error('El usuario no tiene empresa definida');

            return redirect(route('profesionals.index'));
        }        

        if($input['altura']=='')
            $input['altura'] = null;

        $input['id_empresa'] = $user->id_empresa;        

        $profesional = $this->profesionalRepository->create($input);

        Flash::success('Profesional guardado de forma correcta.');

        return redirect(route('profesionals.index'));
    }

    /**
     * Display the specified Profesional.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $profesional = $this->profesionalRepository->find($id);

        if (empty($profesional)) {
            Flash::error('Profesional no encontrado');

            return redirect(route('profesionals.index'));
        }

        return view('profesionals.show')->with('profesional', $profesional);
    }

    /**
     * Show the form for editing the specified Profesional.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $profesional = $this->profesionalRepository->find($id);

        $user=Auth::user();

        $profesiones = Profesion::where(['activo'=>true, 'id_empresa'=>$user->id_empresa])
            ->orderBy('descripcion')
            ->pluck('descripcion', 'id');

        $especialidades = Especialidad::where(['activo'=>true, 'id_empresa'=>$user->id_empresa])
            ->orderBy('descripcion')
            ->pluck('descripcion', 'id');

        if (empty($profesional)) {
            Flash::error('Profesional no encontrado');

            return redirect(route('profesionals.index'));
        }

        return view('profesionals.edit')
            ->with('profesional', $profesional)
            ->with('profesiones', $profesiones)
            ->with('especialidades', $especialidades);
    }

    /**
     * Update the specified Profesional in storage.
     *
     * @param int $id
     * @param UpdateProfesionalRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProfesionalRequest $request)
    {
        $profesional = $this->profesionalRepository->find($id);

        if (empty($profesional)) {
            Flash::error('Profesional no encontrado');

            return redirect(route('profesionals.index'));
        }

        $datos = $request->all();
        
        
        DB::beginTransaction();
        try {
            if(isset($datos['profesiones']))            
                $profesional->profesiones()->sync(array_column($datos['profesiones'],'id_profesion'));        

            if(isset($datos['especialidades']))            
                $profesional->especialidades()->sync(array_column($datos['especialidades'],'id_especialidad'));        

            $profesional = $this->profesionalRepository->update($datos, $id);            

            DB::commit();

            Flash::success('Profesional actualizado de forma correcta.');

            return redirect(route('profesionals.index'));
                
        } catch (Exception $e) {
            DB::rollBack();

            Flash::error('Error al grabar el profesional');

            return redirect(route('profesionals.index'));
        }    

    }

    /**
     * Remove the specified Profesional from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $profesional = $this->profesionalRepository->find($id);

        if (empty($profesional)) {
            Flash::error('Profesional no encontrado');

            return redirect(route('profesionals.index'));
        }

        $this->profesionalRepository->delete($id);

        Flash::success('Profesional borrado de forma correcta.');

        return redirect(route('profesionals.index'));
    }
}
