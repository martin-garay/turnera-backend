<?php

namespace App\Http\Livewire;

use App\Models\Dia;
use Livewire\Component;

class SelectorDiasSemana extends Component
{
    public $days = array();
    public $selectedDays = array();
    public $selectedDaysID = "";
    public $editable = true;

    public function mount($selectedDays = [], $editable=true)
    {
        $this->days = Dia::all();
        $this->selectedDays = array();  //por que el parametro se guarda aca automaticamente

        if(is_string($selectedDays)) 
            $selectedDays = explode(',', $selectedDays);

        foreach ($this->days as $key => $day) {                        
            $this->selectedDays[$day->id] = in_array($day->id, $selectedDays); 
        }                          
    }

    public function render()
    {        
        $selectedDays = array();
        foreach ($this->selectedDays as $key => $check) {
            if($check)
                $selectedDays[] = $key;
        }        
        $this->selectedDaysID = implode(',', $selectedDays);
        
        return view('livewire.selector-dias-semana');
    }

    
}
