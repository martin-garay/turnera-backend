<?php

namespace App\Http\Livewire;

use App\Models\Profesional;
use App\Models\Especialidad;
use Livewire\Component;

class ProfesionalEspecialidad extends Component
{
    public $profesionales;
    public $especialidades;

    public $selectedProfesional = null;
    public $selectedEspecialidad = null;

    public function mount($selectedProfesional = null, $selectedEspecialidad = null)
    {
        $this->profesionales = Profesional::all();
        $this->especialidades = collect();

        $this->selectedProfesional = $selectedProfesional;
        $this->selectedEspecialidad = $selectedEspecialidad;

        if(!is_null($selectedProfesional)){
            $profesional = Profesional::find($selectedProfesional);

            if($profesional){
                $this->especialidades = $profesional->especialidades()->get();
            }
        }
    }

    public function render()
    {
        return view('livewire.profesional-especialidad');
    }

    public function updatedSelectedProfesional($profesional)
    {
        $this->especialidades = collect();
        if(!is_null($profesional) && $profesional!==""){

            $profesional = Profesional::find($profesional);

            if($profesional){
                $this->especialidades = $profesional->especialidades()->get();
            }else{
                $this->especialidades = collect();
            }
            $this->selectedEspecialidad = null;
        }else{
            $this->selectedProfesional = null;  //para que me saque el string vacio y borre la especialidad en la view
        }
    }

    public function updatedSelectedEspecialidad($selectedEspecialidad)
    {                
        $this->selectedEspecialidad = $selectedEspecialidad;

    }
}
