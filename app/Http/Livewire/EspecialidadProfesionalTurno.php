<?php

namespace App\Http\Livewire;

use App\Models\Profesional;
use App\Models\Especialidad;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use App\Models\Turno;
use App\Models\Estados\Turno\Disponible;


class EspecialidadProfesionalTurno extends Component
{
    public $especialidades;
    public $profesionales;
    public $fechas;
    public $turnos;

    public $selectedEspecialidad = null;
    public $selectedProfesional = null;
    public $selectedFecha = null;
    public $selectedTurno = null;
    
    public function mount($selectedEspecialidad = null, $selectedProfesional = null, $turno = null)
    {
        $this->especialidades = Especialidad::all();
        $this->profesionales = collect();
        $this->fechas = collect();
        $this->turnos = collect();

        $this->selectedProfesional = $selectedProfesional;
        $this->selectedEspecialidad = $selectedEspecialidad;

        if($turno){
            $this->selectedFecha = $turno->fecha->format('Y-m-d');

            $this->selectedTurno = $turno->id;
        }

        if(!is_null($selectedEspecialidad)){
            $especialidad = Especialidad::find($selectedEspecialidad);

            if($especialidad){
                $this->profesionales = $especialidad->profesionales()->get();
            }            
        }

        if(!is_null($selectedProfesional)){
            
            $fechas = DB::table('turnos')                                    
                                    ->where('estado', Disponible::$name)
                                    ->where('id_profesional', $selectedProfesional)
                                    ->distinct('fecha')
                                    ->get()
                                    ->pluck('fecha');

            if($fechas){
                $this->fechas = $fechas;
            }            
        }

        if(!is_null($this->selectedFecha)){
            
            $this->turnos = Turno::where([
                'estado' => Disponible::$name,
                'id_especialidad' => $this->selectedEspecialidad,
                'id_profesional' => $this->selectedProfesional,
                'fecha' => $this->selectedFecha
            ])->get();

            //agrego el turno que se reservo
            $turno_reservado = Turno::find($this->selectedTurno);
            if($turno_reservado)
                $this->turnos->push($turno_reservado);
        }
    }

    public function render()
    {
        return view('livewire.especialidad-profesional-turno');
    }

    public function updatedSelectedEspecialidad($especialidad)
    {
        $this->profesionales = collect();
        if(!is_null($especialidad) && $especialidad!==""){

            $especialidad = Especialidad::find($especialidad);

            if($especialidad){
                $this->profesionales = $especialidad->profesionales()->get();
            }else{
                $this->profesionales = collect();
            }            
            $this->selectedProfesional = null;  
        }else{
            $this->selectedEspecialidad = null;  //para que me saque el string vacio y borre la especialidad en la view
        }
    }

    public function updatedSelectedProfesional($selectedProfesional)
    {                
        $this->selectedProfesional = $selectedProfesional;

        $this->fechas = collect();
        if(!is_null($selectedProfesional) && $selectedProfesional!==""){

            $this->fechas = DB::table('turnos')                                    
                                    ->where('estado', Disponible::$name)
                                    ->where('id_especialidad', $this->selectedEspecialidad)
                                    ->where('id_profesional', $selectedProfesional)
                                    ->distinct('fecha')
                                    ->get()
                                    ->pluck('fecha');
            
        }else{
            $this->selectedProfesional = null;  //para que me saque el string vacio y borre la fecha en la view
        }
        
        $this->selectedFecha = null;
        $this->selectedTurno = null;

    }

    public function updatedSelectedFecha($fecha)
    {                
        $this->selectedFecha = $fecha;

        $this->turnos = collect();
        if(!is_null($fecha) && $fecha!==""){

            $this->selectedFecha = $fecha;

            $this->turnos = Turno::where([
                'estado' => Disponible::$name,
                'id_especialidad' => $this->selectedEspecialidad,
                'id_profesional' => $this->selectedProfesional,
                'fecha' => $fecha
            ])->get();           
            
        }else{
            $this->selectedFecha = null;
        }
        
        $this->selectedTurno = null;  
    }

    public function updatedSelectedTurno($turno)
    {                
        $this->selectedTurno = $turno;
    }
}
