<?php

namespace App\Listeners;

use App\Events\ReservaTurno;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class VerificarEstadoTurno
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReservaTurno  $event
     * @return void
     */
    public function handle(ReservaTurno $event)
    {
        $turno = $event->reserva->turno;

        if($turno->sobreturnos==$turnos->getCantidadReservas())
            $turno->estado->transitionTo(Reservado::class, 'Error al cambiar el estado del Turno');
    }
}
