<?php

namespace App\Listeners;

use App\Events\ReservaCancelada;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\MailReservaCancelada;
use Illuminate\Support\Facades\Mail;


class EnviarNotificacionReservaCancelada
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReservaCancelada  $event
     * @return void
     */
    public function handle(ReservaCancelada $event)
    {
        $email = $event->reserva->usuario->email;
        Mail::to($email)->send(new MailReservaCancelada($event->reserva));     
    }
}
