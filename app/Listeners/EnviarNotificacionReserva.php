<?php

namespace App\Listeners;

use App\Events\ReservaCreada;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailReservaCreada;

class EnviarNotificacionReserva
{    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    { 
    }

    /**
     * Handle the event.
     *
     * @param  ReservaCreada  $event
     * @return void
     */
    public function handle(ReservaCreada $event)
    {
        $email = $event->reserva->usuario->email;
        Mail::to($email)->send(new MailReservaCreada($event->reserva));
        
    }
}
