<?php

namespace App\DataTables;

use App\Models\Reserva;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Support\Facades\DB;


class AgendaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'agenda.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ListadoReservas $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Reserva $model)
    {
        return $model->newQuery()
            ->select(['*', DB::raw("concat(profesionales.apellido,' ',profesionales.nombre) as profesional")])
            ->join('users', 'users.id', '=', 'reservas.id_usuario')
            ->join('turnos', 'turnos.id', '=', 'reservas.id_turno')
            ->join('profesionales', 'turnos.id_profesional', '=', 'profesionales.id')
            ->join('especialidades', 'turnos.id_especialidad', '=', 'especialidades.id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())                    
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'responsive' => true,
                'buttons'   => [
                    //['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    //['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    //['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.11.4/i18n/es_es.json'),//<--here
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'fecha',         'name' => 'fecha',          'title' => 'Fecha'],
            ['data' => 'hora_desde',    'name' => 'hora_desde',     'title' => 'Hora'],
            ['data' => 'name',          'name' => 'name',           'title' => 'Nombre usuario'],
            ['data' => 'profesional',   'name' => 'profesional',    'title' => 'Profesional'],            
            ['data' => 'descripcion',   'name' => 'descripcion',    'title' => 'Descripción'],
            ['data' => 'estado',        'name' => 'estado',         'title' => 'Estado'],
        ];        
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'agenda_turnos_' . time();
    }
}
