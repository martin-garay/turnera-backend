<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\ReservaCreada;
use App\Events\ReservaCancelada;
use App\Listeners\EnviarNotificacionReserva;
use App\Listeners\EnviarNotificacionReservaCancelada;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ReservaCreada::class => [        
            EnviarNotificacionReserva::class,
        ],
        ReservaCancelada::class => [        
            EnviarNotificacionReservaCancelada::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
