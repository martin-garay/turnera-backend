<?php

namespace Database\Factories;

use App\Models\Sucursal;
use Illuminate\Database\Eloquent\Factories\Factory;

class SucursalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sucursal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word,
        'calle' => $this->faker->word,
        'altura' => $this->faker->randomDigitNotNull,
        'departamento' => $this->faker->word,
        'piso' => $this->faker->randomDigitNotNull,
        'id_localidad' => $this->faker->randomDigitNotNull,
        'telefono_fijo' => $this->faker->word,
        'telefono_celular' => $this->faker->word,
        'mail' => $this->faker->text,
        'id_empresa' => $this->faker->randomDigitNotNull,
        'orden' => $this->faker->randomDigitNotNull,
        'casa_central' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
