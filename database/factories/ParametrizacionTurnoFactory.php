<?php

namespace Database\Factories;

use App\Models\ParametrizacionTurno;
use Illuminate\Database\Eloquent\Factories\Factory;

class ParametrizacionTurnoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ParametrizacionTurno::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fecha_desde' => $this->faker->word,
        'fecha_hasta' => $this->faker->word,
        'hora_desde' => $this->faker->word,
        'hora_hasta' => $this->faker->word,
        'duracion_turno' => $this->faker->randomDigitNotNull,
        'sobreturnos' => $this->faker->randomDigitNotNull,
        'id_estado' => $this->faker->randomDigitNotNull,
        'id_sucursal' => $this->faker->randomDigitNotNull,
        'id_profesional' => $this->faker->randomDigitNotNull,
        'id_especialidad' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
