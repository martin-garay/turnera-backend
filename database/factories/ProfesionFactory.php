<?php

namespace Database\Factories;

use App\Models\Profesion;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfesionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Profesion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'descripcion	string,200' => $this->faker->text,
        'activo' => $this->faker->word,
        'id_empresa' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
