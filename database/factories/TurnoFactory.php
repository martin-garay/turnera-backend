<?php

namespace Database\Factories;

use App\Models\Turno;
use Illuminate\Database\Eloquent\Factories\Factory;

class TurnoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Turno::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fecha' => $this->faker->word,
        'hora_desde' => $this->faker->word,
        'hora_hasta' => $this->faker->word,
        'id_estado' => $this->faker->randomDigitNotNull,
        'fecha_generacion' => $this->faker->date('Y-m-d H:i:s'),
        'id_profesional' => $this->faker->randomDigitNotNull,
        'id_especialidad' => $this->faker->randomDigitNotNull,
        'id_parametrizacion' => $this->faker->randomDigitNotNull,
        'sobreturnos' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
