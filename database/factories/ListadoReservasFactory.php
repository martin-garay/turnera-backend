<?php

namespace Database\Factories;

use App\Models\ListadoReservas;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListadoReservasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ListadoReservas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'usuario' => $this->faker->text,
        'fecha' => $this->faker->word,
        'hora' => $this->faker->word,
        'profesional' => $this->faker->word,
        'especialidad' => $this->faker->word,
        'estado' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
