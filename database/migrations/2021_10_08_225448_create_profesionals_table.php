<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfesionalsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesionales', function (Blueprint $table) {
            $table->id('id');
            $table->string('nombre', 255);
            $table->string('apellido', 255);
            $table->string('matricula', 255)->nullable();
            $table->integer('dni')->unsigned();
            $table->string('calle', 255)->nullable();
            $table->integer('altura')->unsigned()->nullable();
            $table->string('departamento', 20)->nullable();
            $table->boolean('activo')->default(true);
            $table->integer('id_empresa')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_empresa')->references('id')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesionals');
    }
}
