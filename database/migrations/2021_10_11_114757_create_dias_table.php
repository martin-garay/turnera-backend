<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dias', function (Blueprint $table) {
            $table->id('id');
            $table->string('nombre', 10);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('dias')->insert(
            array(
                ['id' => 1, 'nombre' => 'Lunes'],
                ['id' => 2, 'nombre' => 'Martes'],
                ['id' => 3, 'nombre' => 'Miercoles'],
                ['id' => 4, 'nombre' => 'Jueves'],
                ['id' => 5, 'nombre' => 'Viernes'],
                ['id' => 6, 'nombre' => 'Sabado'],
                ['id' => 7, 'nombre' => 'Domingo'],
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dias');
    }
}
