<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPivotTableProfesionalesEspecialidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesionales_especialidades', function (Blueprint $table) {
            $table->id('id');
            $table->integer('id_profesional')->unsigned();
            $table->integer('id_especialidad')->unsigned();            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_profesional')->references('id')->on('profesionales');
            $table->foreign('id_especialidad')->references('id')->on('especialidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesionales_especialidades');
    }
}
