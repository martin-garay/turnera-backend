<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id('id');
            $table->string('nombre', 200);
            $table->integer('id_rubro')->nullable();
            $table->binary('logo')->nullable();
            $table->string('cuit', 20)->nullable( );
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_rubro')->references('id')->on('rubros');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresas');
    }
}
