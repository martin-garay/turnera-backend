<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            $table->id('id');
            $table->string('nombre', 255);
            $table->string('calle', 255);
            $table->integer('altura')->unsigned();
            $table->string('departamento', 20)->nullable();
            $table->integer('piso')->unsigned()->nullable();
            $table->integer('id_localidad')->unsigned()->nullable();
            $table->string('telefono_fijo', 100)->nullable();
            $table->string('telefono_celular', 100)->nullable();
            $table->text('mail')->nullable();
            $table->integer('id_empresa')->unsigned();
            $table->integer('orden')->unsigned()->nullable();
            $table->boolean('casa_central')->default(false);
            $table->boolean('activo')->default(true);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('id_empresa')->references('id')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sucursales');
    }
}
