<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos', function (Blueprint $table) {
            $table->id('id');
            $table->integer('id_sucursal')->unsigned();
            $table->date('fecha');
            $table->time('hora_desde');
            $table->time('hora_hasta');
            $table->integer('duracion')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->timestamp('fecha_generacion')->useCurrent();
            $table->integer('id_profesional')->unsigned();
            $table->integer('id_especialidad')->unsigned();
            $table->integer('id_parametrizacion')->unsigned()->nullable();
            $table->integer('sobreturnos')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_sucursal')->references('id')->on('sucursales');
            $table->foreign('id_profesional')->references('id')->on('profesionales');
            $table->foreign('id_especialidad')->references('id')->on('especialidades');
            $table->foreign('id_parametrizacion')->references('id')->on('parametrizacion_turnos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('turnos');
    }
}
