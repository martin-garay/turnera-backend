<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametrizacionTurnosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametrizacion_turnos', function (Blueprint $table) {
            $table->id('id');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->time('hora_desde');
            $table->time('hora_hasta');
            $table->string('dias',20);
            $table->integer('duracion_turno')->unsigned();
            $table->integer('sobreturnos')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('id_sucursal')->unsigned();
            $table->integer('id_profesional')->unsigned();
            $table->integer('id_especialidad')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parametrizacion_turnos');
    }
}
