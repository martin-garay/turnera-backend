<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGeneradoToParametrizacionTurnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parametrizacion_turnos', function (Blueprint $table) {
            $table->boolean('generado')->default(false);

            $table->dropColumn('id_estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parametrizacion_turnos', function (Blueprint $table) {
            $table->dropColumn('generado');

            $table->integer('id_estado')->unsigned();
        });
    }
}
