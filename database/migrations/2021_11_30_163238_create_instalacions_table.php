<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstalacionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instalaciones', function (Blueprint $table) {
            $table->id('id');
            $table->string('descripcion', 100);            
            $table->unsignedDecimal('valor_hora', 10, 2)->nullable();
            $table->integer('capacidad_personas')->nullable();
            $table->text('observaciones')->nullable();
            $table->integer('id_tipo_instalacion');
            $table->integer('id_sucursal')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_tipo_instalacion')->references('id')->on('tipo_instalaciones');
            $table->foreign('id_sucursal')->references('id')->on('sucursales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instalacions');
    }
}
