<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPivotTableProfesionalProfesion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesionales_profesiones', function (Blueprint $table) {
            $table->id('id');
            $table->integer('id_profesional')->unsigned();
            $table->integer('id_profesion')->unsigned();            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_profesional')->references('id')->on('profesionales');
            $table->foreign('id_profesion')->references('id')->on('profesiones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profesionales_profesiones');
    }
}
