<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('rubros', App\Http\Controllers\RubroController::class);


Route::resource('empresas', App\Http\Controllers\EmpresaController::class);


Route::resource('profesions', App\Http\Controllers\ProfesionController::class);


Route::resource('profesionals', App\Http\Controllers\ProfesionalController::class);


Route::resource('especialidads', App\Http\Controllers\EspecialidadController::class);


Route::resource('sucursals', App\Http\Controllers\SucursalController::class);


Route::resource('feriados', App\Http\Controllers\FeriadoController::class);


Route::resource('parametrizacionTurnos', App\Http\Controllers\ParametrizacionTurnoController::class);

Route::resource('dias', App\Http\Controllers\DiaController::class);

Route::get('parametrizacionTurnos/{id}/visualizarTurnos', [App\Http\Controllers\ParametrizacionTurnoController::class,'visualizarTurnos'])
    ->name('parametrizacionTurnos.visualizarTurnos');

Route::get('parametrizacionTurnos/{id}/generarTurnos', [App\Http\Controllers\ParametrizacionTurnoController::class,'generarTurnos'])
    ->name('parametrizacionTurnos.generarTurnos');    


Route::resource('turnos', App\Http\Controllers\TurnoController::class);


Route::resource('tipoInstalacions', App\Http\Controllers\TipoInstalacionController::class);


Route::resource('instalacions', App\Http\Controllers\InstalacionController::class);


Route::resource('reservas', App\Http\Controllers\ReservaController::class);

Route::post('reservas/{id}/cancelar', [App\Http\Controllers\ReservaController::class,'cancelar'])->name('reservas.cancelar'); 

Route::resource('agenda', App\Http\Controllers\AgendaController::class);

Route::get('agenda/{id}/ausente', [App\Http\Controllers\AgendaController::class,'ausente'])->name('agenda.ausente');

Route::get('agenda/{id}/presente', [App\Http\Controllers\AgendaController::class,'presente'])->name('agenda.presente');        


Route::resource('users', App\Http\Controllers\UserController::class)->middleware('auth');
